<?php
/**
 * Created by PhpStorm.
 * User: ccarrasco
 * Date: 05/09/2018
 * Time: 09:54 AM
 */

namespace App\Exports;

namespace App\Exports;
use App\Models\Cabecera;
use App\Models\Operativo;
use App\Models\OperativoEstablecimiento;
use App\Models\Participacion;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;


class OperativoAplicadoresExport implements FromCollection, WithMapping, WithHeadings,ShouldAutoSize, WithEvents
{

    /*Este listado es para cada cabecera y para nosotros con el total de las cabeceras
    Se puede descargar en todo momento con la información disponible de ese momento
    Cuando se cierre la evaluación a esta tabla hay que agregarle las siguientes columnas: Participó y Pagar
    CUE ANEXO
    Nombre del establecimiento
    Localidad
    Teléfono
    Año
    Sección
    Turno
    Matrícula (la corregida por el ref de cabecera)
    Nombre Aplicador
    DNI Aplicador
    Nº empleado Aplicador
    Teléfono Aplicador
    email Aplicador
    Cabecera
    Participación 2018
    */

    use Exportable;

    public function __construct(Operativo $operativo, Cabecera $cabecera)
    {
        $this->cabecera=$cabecera->id;
        $this->operativo=$operativo->id;
    }


    public function headings(): array
    {
        return [
            'CUE ANEXO',
            'Nombre del establecimiento',
            'Localidad',
            'Teléfono',
            'Año',
            'Sección',
            'Turno',
            'Matrícula',
            'Nombre Aplicador',
            'DNI Aplicador',
            'Nº empleado Aplicador',
            'Teléfono Aplicador',
            'email Aplicador',
            'Cabecera',
            'Participación 2018',
        ];
    }

    public function collection()
    {
        $operativo=Operativo::find( $this->operativo);


        if ($this->cabecera==null)
        {

            return
                $operativo->secciones()->get();
        }

        else
        {
            $cabecera=Cabecera::find( $this->cabecera);
            return
                $operativo->seccionesDeCabecera($cabecera)->get();
        }


    }

    public function map($seccion): array
    {
        return [
            $seccion->cue,
            $seccion->nombre,
            $seccion->localidad->descripcion,
            $seccion->telefono,
            $seccion->pivot->gradooanio,
            $seccion->pivot->seccion,
            $seccion->pivot->turno,
            $seccion->pivot->matriculaaevaluar,
            $seccion->pivot->aplicador,
            $seccion->pivot->aplicador_dni,
            $seccion->pivot->aplicador_nroempleado,
            $seccion->pivot->aplicador_telefono,
            $seccion->pivot->aplicador_mail,
            Cabecera::where('id', $seccion->pivot->cabecera_id)->first()->descripcion,
            Participacion::where('id',$seccion->pivot->participacion_id)->first()->descripcion
        ];
    }

    public function registerEvents(): array
    {

        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:P1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setBold(true);

            },
        ];


    }

}