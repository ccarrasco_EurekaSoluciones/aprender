<?php

namespace App\Exports;

namespace App\Exports;
use App\Models\Cabecera;
use App\Models\Departamento;
use App\Models\Localidad;
use App\Models\Operativo;
use App\Models\OperativoCabecera;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;


class OperativoCabeceraExport implements FromCollection, WithMapping, WithHeadings,ShouldAutoSize, WithEvents
{
/*Este listado lo vemos nosotros. El referente de cabecera tiene que indicar en el sistema la ubicación de su cabecera
 (ej. Distrito, escuela Nº…) y todo lo que está sombreado
ordenado por numero de cabecera

Código Cabecera
Nombre Cabecera
Ubicación de la cabecera
Dirección
Cod. Postal
Departamento
Localidad
Referente de cabecera
Teléfono
Mail
DNI
Nº de empleado

*/

    use Exportable;

    public function __construct(Operativo $operativo)
    {
        $this->operativo=$operativo->id;
    }


    public function headings(): array
    {
        return [
            'Código Cabecera',
            'Nombre Cabecera',
            'Ubicación de la cabecera',
            'Dirección',
            'Cod. Postal',
            'Departamento',
            'Localidad',
            'Referente de cabecera',
            'Teléfono',
            'Mail',
            'DNI',
            'Nº de empleado',


        ];
    }

    public function collection()
    {
        $operativo=Operativo::find( $this->operativo);



        return
                $operativo->cabecerascondatos()->get();
    }

    public function map($cabecera): array
    {
        return [
            $cabecera->descripcion,
            $cabecera->descripcion,
            $cabecera->pivot->ubicacion,
            $cabecera->pivot->direccion,
            $cabecera->pivot->codigopostal,
            ($cabecera->pivot->departamento_id==null ? '' : Departamento::where('id', $cabecera->pivot->departamento_id)->first()->descripcion),
            ($cabecera->pivot->localidad_id==null ? '' : Localidad::where('id', $cabecera->pivot->localidad_id)->first()->descripcion),
            ($cabecera->user==null ? '':$cabecera->user->nombre." ".$cabecera->user->apellido),
            ($cabecera->user==null ? '':$cabecera->user->telefono),
            ($cabecera->user==null ? '':$cabecera->user->mail),
            ($cabecera->user==null ? '':$cabecera->user->dni),
            ($cabecera->user==null ? '':$cabecera->user->numeroempleado)

        ];
    }

    public function registerEvents(): array
    {

        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:P1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setBold(true);

            },
        ];


    }


}
