<?php
/**
 * Created by PhpStorm.
 * User: ccarrasco
 * Date: 05/09/2018
 * Time: 09:54 AM
 */

namespace App\Exports;

use App\Models\Operativo;
use App\Exports\OperativoPagosExport;
use App\Exports\OperativoEstablecimientosAdminExport;
use App\Exports\OperativoEstadisticaxCabecera;
use App\Exports\ReportExport;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class OperativoEstadisticasParticipacionAdminExport  implements WithMultipleSheets
{

    use Exportable;
    /*
    Necesitamos que de esta tabla se obtengan algunas estadísticas para:
    Total Neuquén,
    por cabecera,
    por distrito,
    por departamento,
    por sector,
    por ámbito,
    por periodo de funciomamiento

    Los datos que necesitamos son:

    Cantidad de establecimientos que participaron,
    que no participaron y el porcentaje de participación para : Participación 2017 y 2018

    Cantidad de secciones de 6º,
    cantidad de secciones evaluadas y porcentaje de secciones evaluadas
    Matricula de 6º, matricula evaluada, porcentaje de matricula evaluada
    */

    public function __construct(Operativo $operativo)
    {
        $this->operativo = $operativo->id;

    }


    public function sheets(): array
    {
        $sheets = [];

        $sheets[] = new OperativoEstadisticaxCabecera (Operativo::find( $this->operativo));
        //$sheets[] = new ReportExport(Operativo::find( $this->operativo));

        return $sheets;
    }

}