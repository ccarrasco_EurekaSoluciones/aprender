<?php
/**
 * Created by PhpStorm.
 * User: ccarrasco
 * Date: 05/09/2018
 * Time: 09:50 AM
 */

namespace App\Exports;
use App\Models\Cabecera;
use App\Models\Operativo;
use App\Models\OperativoEstablecimiento;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

class OperativoEstadisticaxCabecera implements FromCollection, WithMapping, WithHeadings,ShouldAutoSize, WithEvents,WithTitle,WithColumnFormatting
{

    /*Este listado va a tener por cada cabecera las siguientes columnas:
    - Cabecera
    -  Cantidad de establecimientos que participaron,
    - Cantidad de establecimientos que no participaron
    - Prcentaje de participación para : Participación 2017 y 2018

    - Cantidad de secciones de 6º,
    - cantidad de secciones evaluadas
    - porcentaje de secciones evaluadas
     - Matricula ,
    - matricula evaluada,
    - porcentaje de matricula evaluada

    */

    use Exportable;

    public function __construct(Operativo $operativo)
    {
        $this->operativo=$operativo->id;
    }


    public function headings(): array
    {
        return [
            'Cabecera',
            'Cant. Establecimientos',
            'Establec. Participaron',
            'Establec. No Participaron',
            '% Participacion',
            'Cant. Secciones',
            'Secc. Participaron',
            '% Participacion',
            'Matricula',
            'Matricula Evaluada',
            '% Evaluado',
        ];
    }

    public function collection()
    {
        $operativo=Operativo::find( $this->operativo);
        return
            $operativo->cabecerascondatos()->get();
    }

    public function map($cabecera): array
    {
        $operativo=Operativo::find( $this->operativo);

        $establecimientos = $operativo->establecimientosDeCabecera($cabecera)->count();
        $participacionSecciones=$operativo->seccionesParticipaciones($cabecera);
        $participacionEstablecimientos=$operativo->establecimientosParticipaciones($cabecera);

        if ($establecimientos==0)
            $porcentajeestablecimientos=0;
        else
            $porcentajeestablecimientos=($participacionEstablecimientos["SI"]/$establecimientos)*100;

        if ($participacionSecciones["SI"] +$participacionSecciones["NO"]+$participacionSecciones["SD"]+$participacionSecciones["NC"] ==0)
            $porcentajesecciones=0;
        else
            $porcentajesecciones=($participacionSecciones["SI"]/ ($participacionSecciones["SI"] +$participacionSecciones["NO"]+$participacionSecciones["SD"]+$participacionSecciones["NC"]))*100;

        if ($participacionSecciones["matriculaaevaluar"]==0)
            $porcentajematricula=0;
        else
            if ($participacionSecciones["matriculaevaluada"]==0)
                $porcentajematricula=0;
            else
                $porcentajematricula=($participacionSecciones["matriculaevaluada"]/$participacionSecciones["matriculaaevaluar"])*100;

        return [
            $cabecera->descripcion,
            $establecimientos,
            $participacionEstablecimientos["SI"],
            $establecimientos - $participacionEstablecimientos["SI"],
            $porcentajeestablecimientos,
            $participacionSecciones["SI"] +$participacionSecciones["NO"]+$participacionSecciones["SD"]+$participacionSecciones["NC"],
            $participacionSecciones["SI"],
            $porcentajesecciones,
            $participacionSecciones["matriculaaevaluar"],
            $participacionSecciones["matriculaevaluada"],
            $porcentajematricula

        ];
    }

    public function registerEvents(): array
    {

        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:P1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setBold(true);
                $cellRange = 'E2:E100';
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setBold(true);
            },
        ];


    }

    public function title(): string
    {
        return 'Cabeceras';
    }

    public function columnFormats(): array
    {
        return [
            'E' => '0.00',
            'H' => '0.00',
            'K' => '0.00'
        ];

    }

}

