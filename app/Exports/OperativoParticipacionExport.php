<?php
/**
 * Created by PhpStorm.
 * User: ccarrasco
 * Date: 05/09/2018
 * Time: 09:54 AM
 */

namespace App\Exports;

use App\Models\Operativo;
use App\Models\Cabecera;
use App\Models\Participacion;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class OperativoParticipacionExport  implements FromCollection, WithMapping, WithHeadings,ShouldAutoSize, WithEvents
{

    /*Este listado es para cada cabecera y para nosotros con el total de las cabeceras
    Se puede ver cuando finaliza la evaluación

    Necesitamos que de esta tabla se obtengan algunas estadísticas para: Total Neuquén, por cabecera, por distrito, por departamento, por sector, por ámbito, por periodo de funciomamiento

    Los datos que necesitamos son:

    Cantidad de establecimientos que participaron, que no participaron y el porcentaje de participación para : Participación 2017 y 2018
    Cantidad de secciones de 6º, cantidad de secciones evaluadas y porcentaje de secciones evaluadas
    Matricula de 6º, matricula evaluada, porcentaje de matricula evaluada

    CUE ANEXO
    Nombre del establecimiento
    Localidad
    Departamento
    Distrito
    Nivel
    Sector
    Ámbito
    Periodo de funcionamiento
    Participación 2017
    Participación 2018
    Cant de secciones 6º grado
    Cant. De secciones evaluadas
    Matrícula de 6º grado (la corregida por el ref de cabecera)
    Matricula evaluada
    Cabecera

    */

    use Exportable;

    public function __construct(Operativo $operativo, Cabecera $cabecera)
    {
        if ($cabecera !=null)
            $this->cabecera=$cabecera->id;
        else
            $this->cabecera=null;

        $this->operativo=$operativo->id;
    }

    public function headings(): array
    {
        return [
            'CUE ANEXO',
            'Nombre del establecimiento',
            'Localidad',
            'Departamento',
            'Distrito',
            'Nivel',
            'Sector',
            'Ámbito',
            'Periodo de funcionamiento',
            'Participación 2017',
            'Participación 2018',
            'Cant de secciones 6º grado',
            'Cant. De secciones evaluadas',
            'Matrícula de 6º grado',
            'Matricula evaluada',
            'Cabecera',
        ];
    }

    public function collection()
    {
        $operativo=Operativo::find( $this->operativo);


        if ($this->cabecera==null)
        {

            return
                $operativo->establecimientos()->get();
        }

        else
        {
            $cabecera=Cabecera::find( $this->cabecera);
            return
                $operativo->establecimientosDeCabecera($cabecera)->get();
        }


    }

    public function map($establecimiento): array
    {
        return [
            $establecimiento->cue,
            $establecimiento->nombre,
            $establecimiento->localidad->descripcion,
            $establecimiento->departamento->descripcion,
            $establecimiento->distrito,
            $establecimiento->nivel->descripcion,
            $establecimiento->sector->descripcion,
            $establecimiento->ambito->descripcion,
            $establecimiento->periodofuncionamiento->descripcion,
            "S/D",
            Participacion::where('id', $establecimiento->pivot->participacion_id)->first()->descripcion,
            Operativo::where('id',  $this->operativo)->first()->seccionesDeEstablecimientoporid($establecimiento->id)->count(),
            Operativo::where('id',  $this->operativo)->first()->seccionesDeEstablecimientoporid($establecimiento->id)->where('participacion_id','1')->count(),
            Operativo::where('id',  $this->operativo)->first()->seccionesDeEstablecimientoporid($establecimiento->id)->where('participacion_id','<>','4')->sum('matriculaaevaluar'),
            Operativo::where('id',  $this->operativo)->first()->seccionesDeEstablecimientoporid($establecimiento->id)->where('participacion_id','1')->sum('matriculaevaluada'),
            $establecimiento->cabecera->descripcion
        ];
    }

    public function registerEvents(): array
    {

        return [
            AfterSheet::class => function (AfterSheet $event) {
                $cellRange = 'A1:P1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setBold(true);

            },
        ];


    }
}