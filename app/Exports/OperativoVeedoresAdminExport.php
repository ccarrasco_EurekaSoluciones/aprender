<?php
/**
 * Created by PhpStorm.
 * User: ccarrasco
 * Date: 05/09/2018
 * Time: 09:52 AM
 */


namespace App\Exports;
use App\Models\Cabecera;
use App\Models\Operativo;
use App\Models\OperativoEstablecimiento;
use App\Models\Participacion;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class OperativoVeedoresAdminExport implements FromCollection, WithMapping, WithHeadings,ShouldAutoSize, WithEvents
{


    /*Este listado es para cada cabecera y para nosotros con el total de las cabeceras
    Se puede descargar en todo momento con la información disponible de ese momento
    Cuando se cierre la evaluación a esta tabla hay que agregarle las siguientes columnas: Participó y Pagar

    CUE ANEXO
    Nombre del establecimiento
    Localidad
    Teléfono
    Año
    Nombre Veedor
    DNI  Veedor
    Nº empleado  Veedor
    Teléfono  Veedor
    email  Veedor
    Cabecera
    Participación 2017
    Participación 2018

    */
    use Exportable;

    public function __construct(Operativo $operativo)
    {
        $this->operativo=$operativo->id;
    }

    public function headings(): array
    {
        return [
            'CUE ANEXO',
            'Nombre del establecimiento',
            'Localidad',
            'Teléfono',
            'Año',
            'Nombre Veedor',
            'DNI  Veedor',
            'Nº empleado  Veedor',
            'Teléfono  Veedor',
            'email  Veedor',
            'Cabecera',
            'Participación',

        ];
    }

    public function collection()
    {
        $operativo=Operativo::find( $this->operativo);


            return
                $operativo->establecimientos()->get();

    }

    public function map($establecimiento): array
    {
        return [
            $establecimiento->cue,
            $establecimiento->nombre,
            $establecimiento->localidad->descripcion,
            $establecimiento->telefono,
            "2018",
            $establecimiento->pivot->veedor,
            $establecimiento->pivot->veedor_dni,
            $establecimiento->pivot->veedor_nroempleado,
            $establecimiento->pivot->veedor_telefono,
            $establecimiento->pivot->veedor_mail,
            Cabecera::where('id',  $establecimiento->pivot->cabecera_id)->first()->descripcion,
            Participacion::where('id', $establecimiento->pivot->participacion_id)->first()->descripcion
        ];
    }

    public function registerEvents(): array
    {

        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:P1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setBold(true);

            },
        ];


    }
}