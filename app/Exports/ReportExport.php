<?php
/**
 * Created by PhpStorm.
 * User: ccarrasco
 * Date: 05/09/2018
 * Time: 09:50 AM
 */

namespace App\Exports;
use App\Models\Cabecera;
use App\Models\Operativo;
use App\Models\OperativoEstablecimiento;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithTitle;

class ReportExport implements FromCollection, WithMapping, WithHeadings,ShouldAutoSize, WithEvents,WithTitle
{

    /*Este listado es para cada cabecera y para nosotros con el total de las cabeceras
    Se puede descargar en todo momento con la información disponible de ese momento

    CUE ANEXO
    Nombre del establecimiento
    Localidad
    Departamento
    Domicilio
    Teléfono
    mail
    Distrito
    Nivel
    Sector
    Ámbito
    Periodo de funcionamiento
    Cant de secciones  6º grado
    Cabecera
    Nombre del Director
    DNI Director

    */

    use Exportable;

    public function __construct(Operativo $operativo)
    {

        $this->operativo=$operativo->id;
    }


    public function headings(): array
    {
        return [
            'CUE ANEXO',
            'Nombre del establecimiento',
            'Localidad',
            'Departamento',
            'Domicilio',
            'Teléfono',
            'mail',
            'Distrito',
            'Nivel',
            'Sector',
            'Ámbito',
            'Periodo de funcionamiento',
            'Cant de secciones',
            'Cabecera',
            'Nombre del Director',
            'Telefono Director',

        ];
    }

    public function collection()
    {
        $operativo=Operativo::find( $this->operativo);



            return
                $operativo->establecimientos()->get();


    }

    public function map($establecimiento): array
    {
        return [
            $establecimiento->cue,
            $establecimiento->nombre,
            $establecimiento->localidad->descripcion,
            $establecimiento->departamento->descripcion,
            $establecimiento->domicilio,
            $establecimiento->telefono,
            $establecimiento->email,
            $establecimiento->distrito,
            $establecimiento->nivel->descripcion,
            $establecimiento->sector->descripcion,
            $establecimiento->ambito->descripcion,
            $establecimiento->periodofuncionamiento->descripcion,
            Operativo::where('id',  $this->operativo)->first()->seccionesDeEstablecimientoporid($establecimiento->id)->count(),
            $establecimiento->cabecera->descripcion,
            $establecimiento->pivot->director,
            $establecimiento->pivot->directortelefono,
        ];
    }

    public function registerEvents(): array
    {

        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:P1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setBold(true);

            },
        ];


    }

    public function title(): string
    {
        return 'Prueba2';
    }

}

