<?php
/**
 * Created by PhpStorm.
 * User: ccarrasco
 * Date: 05/09/2018
 * Time: 10:11 AM
 */

namespace App\Exports;

use App\Models\Cabecera;
use App\User;
use App\Models\Operativo;
use App\Models\OperativoEstablecimiento;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithTitle;

class ReportSummaryExport implements FromCollection, WithHeadings,ShouldAutoSize, WithEvents,WithTitle
{

    /*Este listado es para nosotros. Se generá cuando se cierra el operativo
    El listado se conforma con todos los referentes de cabecera y aquellos aplicaeores y veedores que se le marca el "pagar"
    DNI
    Nº de empleado
    Apellido y Nombre
    Nº de teléfono
    Rol en Aprender (acá ingresar si fue aplicador, veedor, referente de cabecera)
    Cantidad  aplicaciones (para ref. de cabecera y veedor va 1, si fue aplicador la cantidad de aplicaciones que hizo)
    Monto a pagar (vemos si esto se puede ingresar en algún lugar y luego traerlo a este listado)


    */

    use Exportable;


    public function __construct(Operativo $operativo)
    {
        $this->operativo=$operativo->id;
    }

    public function collection()
    {

        $operativo=Operativo::find( $this->operativo);

        /*Primero tengo que tomar todos los coordinadores de cabecera*/
        $cabeceras = $operativo->cabecerascondatos()->whereNotNull('operativos_cabeceras.user_id')->get();
        $paymentsArray = [];

        // Convert each member of the returned collection into an array,
        // and append it to the payments array.
        foreach ($cabeceras as $coordinador) {
            $user=User::where('id', $coordinador->user_id)->first();

            $paymentsArray[] =  [
                'DNI' => $user->dni,
                'nroEmpleado' =>  $user->numeroempleado,
                'ApellidoyNombre' =>  $user->apellido . " ". $user->nombre,
                'Telefono' =>  $user->telefono,
                'Rol'=> "Coordinador",
                'CantidadAplicaciones'=> 1
            ];
        }

        /*ahora vamos con los veedores*/
        $veedores= $operativo->establecimientos()->wherePivot('pagar',1)->get();
        foreach ($veedores as $veedor) {
            $paymentsArray[] =  [
                'DNI' => $veedor->pivot->veedor_dni,
                'nroEmpleado' =>  $veedor->pivot->veedor_nroempleado,
                'ApellidoyNombre' => $veedor->pivot->veedor,
                'Telefono' =>  $veedor->pivot->veedor_telefono,
                'Rol'=> "Veedor",
                'CantidadAplicaciones'=> 1

            ];
        }

        /*ahora vamos con los aplicadores
        Esto es un poco mas dificil porque puede haber aplicado en mas de un lugar...
        voy a tener que buscar en el arreglo si ya está*/
        $aplicadores= $operativo->secciones()->wherePivot('pagar',1)->get();

        foreach ($aplicadores as $aplicador) {
            $key = array_search($aplicador->pivot->aplicador_dni,  array_column($paymentsArray, 'DNI') );
            if ($key>0)
            {
                $payment=$paymentsArray[$key];
                $payment['CantidadAplicaciones']=$payment['CantidadAplicaciones'] +1 ;
                $paymentsArray[$key]=$payment;
            }
            else
            {
                $paymentsArray[] =  [
                    'DNI' => $aplicador->pivot->aplicador_dni,
                    'nroEmpleado' =>  $aplicador->pivot->aplicador_nroempleado,
                    'ApellidoyNombre' => $aplicador->pivot->aplicador,
                    'Telefono' =>  $aplicador->pivot->aplicador_telefono,
                    'Rol'=> "Aplicador",
                    'CantidadAplicaciones'=> 1

                ];

            }
        }
        return collect($paymentsArray);
    }

    public function headings(): array
    {
        return [
            'DNI',
            'Nº de empleado',
            'Apellido y Nombre',
            'Nº de teléfono',
            'Rol en Aprender',
            'Cantidad  aplicaciones'
        ];
    }

    public function registerEvents(): array
    {

        return [
            AfterSheet::class => function (AfterSheet $event) {
                $cellRange = 'A1:P1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setBold(true);

            },
        ];


    }

    public function title(): string
    {
        return 'Prueba1';
    }
}
