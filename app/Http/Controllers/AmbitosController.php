<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ambito;

class AmbitosController extends Controller
{

    public function index()
    {
        $data =  Ambito::all();
        return view('ambitos.index')->with('ambitos', $data)->with('title','Ambitos');
    }

    public function create()
    {
        return view('ambitos.create')->with('title','Crear un Nuevo Ámbito');
    }

    public function store()
    {
        /*$data=request().validate(
            ['descripcion'=> 'required']
            );*/
        $data = request()->all();


        Ambito::create(['descripcion'=>$data['descripcion']]);
        return redirect()->route('ambitos');
    }

    public function edit(Ambito $ambito)
    {
        return view('ambitos.edit', ['ambito' => $ambito])->with('title','Modificar Ambito');
    }

    public function update(Ambito $ambito)
    {
        /*$data = request()->validate([
            'name' => 'required',
            'email' => ['required', 'email', Rule::unique('users')->ignore($user->id)],
            'password' => '',
        ]);*/

        $data = request()->all();

        $ambito->update($data);
        return redirect()->route('ambitos', ['ambito' => $ambito]);

    }

    function destroy(Ambito $ambito)
    {
        $ambito->delete();
        return redirect()->route('ambitos');
    }

}
