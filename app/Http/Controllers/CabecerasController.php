<?php

namespace App\Http\Controllers;

use App\Models\Operativo;
use App\Models\OperativoCabecera;
use App\Models\OperativoEstablecimiento;
use App\User;
use Illuminate\Http\Request;
use App\Models\Cabecera;

class CabecerasController extends Controller
{

    public function index()
    {
        $data =  Cabecera::all();
        return view('cabeceras.index')->with('cabeceras', $data)->with('title','Cabeceras');
    }

    public function create()
    {
        return view('cabeceras.create')->with('title','Crear una Nueva Cabecera');
    }

    public function store()
    {
        /*$data=request().validate(
            ['descripcion'=> 'required']
            );*/
        $data = request()->all();


        Cabecera::create(['descripcion'=>$data['descripcion'], 'localidad'=>$data['localidad']]);
        return redirect()->route('cabeceras');
    }

    public function edit(Cabecera $cabecera)
    {
        $users = User::where('esadmin',0)->orderBy('apellido')->get()->pluck('nombre_and_apellido', 'id')->toArray();


        if ($cabecera->user!=null) {
            $user = $cabecera->user->id;

        }
        else
            $user=null;

        return view('cabeceras.edit', ['cabecera' => $cabecera])
            ->with('users',$users)
            ->with('usuario',$user)
            ->with('title','Modificar Cabecera');
    }

    public function update(Cabecera $cabecera)
    {
        $data = request()->all();

        $cabecera->update($data);
        /*y tengo que actualizar todos los establecimientos de la tabla de
        operativos_establecimientos para asignarle el usuario*/

        $operativoactual=Operativo::where('actual','1')->first();

        OperativoEstablecimiento::where('cabecera_id',$cabecera->id )
            ->where('operativo_id', $operativoactual->id)
            ->update(array('user_id' => $data['user_id']));

        OperativoCabecera::where('cabecera_id',$cabecera->id )
        ->where('operativo_id', $operativoactual->id)
        ->update(array('user_id' => $data['user_id']));

        return redirect()->route('cabeceras', ['cabecera' => $cabecera]);

    }

    function destroy(Cabecera $cabecera)
    {
        $cabecera->delete();
        return redirect()->route('cabeceras');
    }

}
