<?php

namespace App\Http\Controllers;

use App\Models\Departamento;
use Illuminate\Http\Request;

class DepartamentosController extends Controller
{

    public function index()
    {
        $data = Departamento::all();
        return view('departamentos.index')->with('departamentos', $data)->with('title', 'Ambitos');
    }

    public function create()
    {
        return view('departamentos.create')->with('title', 'Crear un Nuevo Ámbito');
    }

    public function store()
    {
        /*$data=request().validate(
            ['descripcion'=> 'required']
            );*/
        $data = request()->all();


        Departamento::create(['descripcion' => $data['descripcion']]);
        return redirect()->route('departamentos');
    }

    public function edit(Departamento $departamento)
    {
        return view('departamentos.edit', ['departamento' => $departamento])->with('title', 'Modificar Departamento');
    }

    public function update(Departamento $departamento)
    {
        /*$data = request()->validate([
            'name' => 'required',
            'email' => ['required', 'email', Rule::unique('users')->ignore($user->id)],
            'password' => '',
        ]);*/

        $data = request()->all();

        $departamento->update($data);
        return redirect()->route('departamentos', ['departamento' => $departamento]);

    }

    function destroy(Departamento $departamento)
    {
        $departamento->delete();
        return redirect()->route('departamentos');
    }
}