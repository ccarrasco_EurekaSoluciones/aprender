<?php

namespace App\Http\Controllers;

use App\Models\Ambito;
use App\Models\Cabecera;
use App\Models\Departamento;
use App\Models\Modalidad;
use App\Models\Nivel;
use App\Models\Operativo;
use App\Models\OperativoEstablecimiento;
use App\Models\OperativoSeccion;
use App\Models\PeriodoFuncionamiento;
use App\Models\Sector;
use Illuminate\Http\Request;
use App\Models\Establecimiento;
use App\Models\Localidad;
use League\Flysystem\Adapter\Local;


class EstablecimientosController extends Controller
{

    public function index(Request $request)
    {
        /*obtengo los parametros por get que eligieron en la busqueda*/
        $nombre=$request->get('nombre');
        $localidad=$request->get('localidad');
        $nivel=$request->get('nivel');
        $ambito=$request->get('ambito');
        $departamento=$request->get('departamento');
        $sector=$request->get('sector');
        $pf=$request->get('pf');
        $modalidad=$request->get('modalidad');
        $cue=$request->get('cue');
        $cabecera=$request->get('cabecera');

        /*obtengo los objetos para llenar los combos*/
        //$localidades=Localidad::all();

        $localidades = Localidad::orderBy('descripcion')->pluck('descripcion', 'id')->toArray();
        $niveles=Nivel::orderBy('descripcion')->pluck('descripcion', 'id')->toArray();
        $ambitos=Ambito::orderBy('descripcion')->pluck('descripcion', 'id')->toArray();
        $departamentos=Departamento::orderBy('descripcion')->pluck('descripcion', 'id')->toArray();
        $sectores=Sector::orderBy('descripcion')->pluck('descripcion', 'id')->toArray();
        $pfs=PeriodoFuncionamiento::orderBy('descripcion')->pluck('descripcion', 'id')->toArray();
        $modalidades=Modalidad::orderBy('descripcion')->pluck('descripcion', 'id')->toArray();
        $cabeceras=Cabecera::orderBy('descripcion')->pluck('descripcion', 'id')->toArray();

        /*Aplico los filtros usando los scopes que defini en el model*/
        $data =  Establecimiento::nombre($nombre)
                    ->cue($cue)
                    ->localidad($localidad)
                    ->nivel($nivel)
                    ->ambito($ambito)
                    ->departamento($departamento)
                    ->sector($sector)
                    ->periodofuncionamiento($pf)
                    ->modalidad($modalidad)
                    ->cabecera($cabecera)
                    ->orderBy('nombre', 'ASC')->paginate();

        return view('establecimientos.index')
            ->with('establecimientos', $data)
            ->with('title','Establecimientos')
            ->with('localidades', $localidades)
            ->with('niveles', $niveles)
            ->with('sectores', $sectores)
            ->with('ambitos', $ambitos)
            ->with('departamentos',$departamentos)
            ->with('modalidades',$modalidades)
            ->with('cabeceras',$cabeceras)
            ->with('pfs', $pfs);

    }
    public function create()
    {
        $localidades = Localidad::orderBy('descripcion')->pluck('descripcion', 'id')->toArray();
        $niveles=Nivel::orderBy('descripcion')->pluck('descripcion', 'id')->toArray();
        $ambitos=Ambito::orderBy('descripcion')->pluck('descripcion', 'id')->toArray();
        $departamentos=Departamento::orderBy('descripcion')->pluck('descripcion', 'id')->toArray();
        $sectores=Sector::orderBy('descripcion')->pluck('descripcion', 'id')->toArray();
        $pfs=PeriodoFuncionamiento::orderBy('descripcion')->pluck('descripcion', 'id')->toArray();
        $modalidades=Modalidad::orderBy('descripcion')->pluck('descripcion', 'id')->toArray();
        $cabeceras=Cabecera::orderBy('descripcion')->pluck('descripcion', 'id')->toArray();

        return view('establecimientos.create')
            ->with('title','Crear un Nuevo Establecimiento')
            ->with('localidades', $localidades)
            ->with('niveles', $niveles)
            ->with('sectores', $sectores)
            ->with('ambitos', $ambitos)
            ->with('departamentos',$departamentos)
            ->with('modalidades',$modalidades)
            ->with('cabeceras',$cabeceras)
            ->with('pfs', $pfs);
    }

    public function store()
    {
        /*$data=request().validate(
            ['descripcion'=> 'required']
            );*/
        $data = request()->all();


        Establecimiento::create([
            'cue'=>$data['cue'],
            'nombre'=>$data['nombre'],
            'localidad_id'=>$data['localidad'],
            'departamento_id'=>$data['departamento'],
            'domicilio'=>$data['domicilio'],
            'telefono'=>$data['telefono'],
            'email'=>$data['email'],
            'sector_id'=>$data['sector'],
            'ambito_id'=>$data['ambito'],
            'periodofuncionamiento_id'=>$data['pf'],
            'nivel_id'=>$data['nivel'],
            'modalidad_id'=>$data['modalidad'],
            'distrito'=>$data['distrito'],
            'codsupervisor'=>$data['codsupervisor'],
            'nombresupervisor'=>$data['nombresupervisor'],
            'cabecera_id'=>$data['cabecera']
        ]);
        return redirect()->route('establecimientos');
    }

    public function edit(Establecimiento $establecimiento)
    {
        $localidades = Localidad::orderBy('descripcion')->pluck('descripcion', 'id')->toArray();
        $niveles=Nivel::orderBy('descripcion')->pluck('descripcion', 'id')->toArray();
        $ambitos=Ambito::orderBy('descripcion')->pluck('descripcion', 'id')->toArray();
        $departamentos=Departamento::orderBy('descripcion')->pluck('descripcion', 'id')->toArray();
        $sectores=Sector::orderBy('descripcion')->pluck('descripcion', 'id')->toArray();
        $pfs=PeriodoFuncionamiento::orderBy('descripcion')->pluck('descripcion', 'id')->toArray();
        $modalidades=Modalidad::orderBy('descripcion')->pluck('descripcion', 'id')->toArray();
        $cabeceras=Cabecera::orderBy('descripcion')->pluck('descripcion', 'id')->toArray();

        return view('establecimientos.edit', ['establecimiento' => $establecimiento])
            ->with('title','Modificar Establecimiento')
            ->with('localidades', $localidades)
            ->with('niveles', $niveles)
            ->with('sectores', $sectores)
            ->with('ambitos', $ambitos)
            ->with('departamentos',$departamentos)
            ->with('modalidades',$modalidades)
            ->with('cabeceras',$cabeceras)
            ->with('pfs', $pfs);
    }

    public function update(Establecimiento $establecimiento)
    {
        /*$data = request()->validate([
            'name' => 'required',
            'email' => ['required', 'email', Rule::unique('users')->ignore($user->id)],
            'password' => '',
        ]);*/

        $data = request()->all();

        $establecimiento->update($data);

        $operativoactual=Operativo::where('actual','1')->first();

        /*tengo que cambiar la cabecera para el operativo actual*/
        if ($operativoactual!=null )
        {

            $cabecera= Cabecera::where('id',$data['cabecera_id'])->first();

            OperativoSeccion::where('operativo_id', $operativoactual->id)
                ->where('establecimiento_id', $establecimiento->id)
                ->update(array('cabecera_id'=>$data['cabecera_id']));


            OperativoEstablecimiento::where('operativo_id', $operativoactual->id)
                ->where('establecimiento_id', $establecimiento->id)
                ->update(array('cabecera_id'=> $data['cabecera_id'],
                    'user_id'=> $cabecera->user_id));



        }



        return redirect()->route('establecimientos', ['establecimiento' => $establecimiento]);

    }

    function destroy(Establecimiento $establecimiento)
    {
        $establecimiento->delete();
        return redirect()->route('establecimiento');
    }


}
