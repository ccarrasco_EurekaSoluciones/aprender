<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Localidad;

class LocalidadesController extends Controller
{

    public function index()
    {
        $data =  Localidad::all();
        return view('localidades.index')->with('localidades', $data)->with('title','Localidades');
    }

    public function create()
    {
        return view('localidades.create')->with('title','Crear una Nueva Localidad');
    }

    public function store()
    {
        /*$data=request().validate(
            ['descripcion'=> 'required']
            );*/
        $data = request()->all();


        Localidad::create([
                'descripcion'=>$data['descripcion'],
                'caracteristicatelefonica'=>$data['caracteristicatelefonica']
                ]);
        return redirect()->route('localidades');
    }

    public function edit(Localidad $localidad)
    {
        return view('localidades.edit', ['localidad' => $localidad])->with('title','Modificar Localidad');
    }

    public function update(Localidad $localidad)
    {
        /*$data = request()->validate([
            'name' => 'required',
            'email' => ['required', 'email', Rule::unique('users')->ignore($user->id)],
            'password' => '',
        ]);*/

        $data = request()->all();

        $localidad->update($data);
        return redirect()->route('localidades', ['localidad' => $localidad]);

    }

    function destroy(Localidad $localidad)
    {
        $localidad->delete();
        return redirect()->route('localidades');
    }


}
