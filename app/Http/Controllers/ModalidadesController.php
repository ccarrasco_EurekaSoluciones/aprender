<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Modalidad   ;

class ModalidadesController extends Controller
{

    public function index()
    {
        $data =  Modalidad::all();
        return view('modalidades.index')->with('modalidades', $data)->with('title','Modalidades');
    }

    public function create()
    {
        return view('modalidades.create')->with('title','Crear una Nueva Modalidad');
    }

    public function store()
    {
        /*$data=request().validate(
            ['descripcion'=> 'required']
            );*/
        $data = request()->all();


        Modalidad::create(['descripcion'=>$data['descripcion']]);
        return redirect()->route('modalidades');
    }

    public function edit(Modalidad $modalidad)
    {
        return view('modalidades.edit', ['modalidad' => $modalidad])->with('title','Modificar Modalidad');
    }

    public function update(Modalidad $modalidad)
    {
        /*$data = request()->validate([
            'name' => 'required',
            'email' => ['required', 'email', Rule::unique('users')->ignore($user->id)],
            'password' => '',
        ]);*/

        $data = request()->all();

        $modalidad->update($data);
        return redirect()->route('modalidades', ['modalidad' => $modalidad]);

    }

    function destroy(Modalidad $modalidad)
    {
        $modalidad->delete();
        return redirect()->route('modalidades');
    }

}
