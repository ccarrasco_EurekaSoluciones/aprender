<?php

namespace App\Http\Controllers;

use App\Models\Nivel;
use Illuminate\Http\Request;

class NivelesController extends Controller
{
    public function index()
    {
        $data =  Nivel::all();
        return view('niveles.index')->with('niveles', $data)->with('title','Niveles Educativos');
    }

    public function create()
    {
        return view('niveles.create')->with('title','Crear un Nuevo Nivel');
    }

    public function store()
    {
        /*$data=request().validate(
            ['descripcion'=> 'required']
            );*/
        $data = request()->all();


        Nivel::create(['descripcion'=>$data['descripcion']]);
        return redirect()->route('niveles');
    }

    public function edit(Nivel $nivel)
    {
        return view('niveles.edit', ['nivel' => $nivel])->with('title','Modificar Nivel');
    }

    public function update(Nivel $nivel)
    {
        /*$data = request()->validate([
            'name' => 'required',
            'email' => ['required', 'email', Rule::unique('users')->ignore($user->id)],
            'password' => '',
        ]);*/

        $data = request()->all();

        $nivel->update($data);
        return redirect()->route('niveles', ['nivel' => $nivel]);

    }

    function destroy(Nivel $nivel)
    {
        $nivel->delete();
        return redirect()->route('niveles');
    }


}
