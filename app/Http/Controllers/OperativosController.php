<?php

namespace App\Http\Controllers;

use App\Exports\OperativoAplicadoresAdminExport;
use App\Exports\OperativoCabeceraExport;
use App\Exports\OperativoEstablecimientosAdminExport;
use App\Exports\OperativoEstablecimientosExport;
use App\Exports\OperativoAplicadoresExport;
use App\Exports\OperativoVeedoresAdminExport;
use App\Exports\OperativoVeedoresExport;
use App\Exports\OperativoParticipacionExport;
use App\Exports\OperativoPagosExport;
use App\Exports\OperativoParticipacionAdminExport;
use App\Exports\OperativoEstadisticasParticipacionAdminExport;
use App\Models\Cabecera;
use App\Models\OperativoCabecera;
use App\Models\OperativoEstablecimiento;
use App\Models\OperativoSeccion;
use App\Models\Participacion;
use Illuminate\Http\Request;
use App\Models\Operativo;
use App\Models\Establecimiento;

class OperativosController extends Controller
{
    public function index()
    {
        $data =  Operativo::all();
        return view('operativos.index')->with('operativos', $data)->with('title','Operativos');

       /*Excel::load('public/Secciones2017.xls', function($archivo)
       {
           $result=$archivo->get();
           foreach($result as $key => $value)
           {
               echo $value->cue_anexo.''.'<br>';
           }
       })->get();*/
    }

    public function create()
    {
        return view('operativos.create')->with('title','Crear un Nuevo Operativo');
    }

    public function store()
    {
        /*$data=request().validate(
            ['descripcion'=> 'required']
            );*/
        $data = request()->all();


        Operativo::create(['nombre'=>$data['nombre'],
            'grado'=>$data['grado'],
            'anio'=>$data['anio'],
            'materiasgrado'=>$data['materiasgrado'],
            'materiasanio'=>$data['materiasanio'],
            'fecha'=>$data['fecha']]);

        return redirect()->route('operativos');
    }

    public function edit(Operativo $operativo)
    {
        /*Acá tengo que buscar las estadisticas que necesitan de cada opeartivo*/

        $establecimientos = $operativo->establecimientosDeOperativo($operativo)->get();
        $secciones=$operativo->seccionesDeOperativo($operativo);

        $establecimientosnivelprimario=$establecimientos->where('nivel_id','1')->count();
        $establecimientosnivelsecundario=$establecimientos->where('nivel_id','2')->count();

        $seccionesNivelPrimario=$operativo->seccionesDePrimariadeOperativo($operativo)->count();
        $seccionesNivelSecundario=$secciones->count()-$seccionesNivelPrimario;

        $participacionSecciones=$operativo->seccionesParticipacionesenOperativo($operativo);
        $participacionEstablecimientos=$operativo->establecimientosParticipacionesenOperativo($operativo);


        $cabeceras=$operativo->cabeceras()->distinct('id')->get();

        return view('operativos.edit', ['operativo' => $operativo])
            ->with('establecimientosnivelprimario',$establecimientosnivelprimario)
            ->with('establecimientosnivelsecundario',$establecimientosnivelsecundario)
            ->with('establecimientos_cantidad',$establecimientos->count())
            ->with('establecimientos',$establecimientos)
            ->with('seccionesnivelprimario',$seccionesNivelPrimario)
            ->with('seccionesnivelsecundario',$seccionesNivelSecundario)
            ->with('secciones_cantidad',$secciones->count())
            ->with('cabeceras',$cabeceras)
            ->with('participacionSecciones',$participacionSecciones)
            ->with('participacionEstablecimientos',$participacionEstablecimientos)
            ->with('title','Modificar Operativo');

    }



    public static function mostrarOperativo(Operativo $operativo )
    {
        /*Si estoy aca es porque vengo de un vinculo "Aprender XXXX pero no tengo la cabecera
        porque puede ser que sea de un admin o de un coordinador de cabecera,
        asi que tengo que ver quien es para llamar a un form u otro*/

        if (auth()->user()->esAdmin)
        {
            $cabecera=auth()->user()->cabecera();
            return self::mostrarDatosOperativo($operativo);

        }
        else
        {

            $cabecera=auth()->user()->cabecera()->first();
            return self::mostrarDatosCabecera($operativo, $cabecera);

        }

    }

    /*Muestra solo los datos de una cabecera*/
    public static function mostrarDatosCabecera(Operativo $operativo, Cabecera $cabecera )
    {
        $establecimientos = $operativo->establecimientosDeCabecera($cabecera)->get();
        $secciones=$operativo->seccionesDeCabecera($cabecera);

        $establecimientosnivelprimario=$establecimientos->where('nivel_id','1')->count();
        $establecimientosnivelsecundario=$establecimientos->where('nivel_id','2')->count();

        $seccionesNivelPrimario=$operativo->seccionesDePrimaria($cabecera)->count();
        $seccionesNivelSecundario=$secciones->count()-$seccionesNivelPrimario;

        $participacionSecciones=$operativo->seccionesParticipaciones($cabecera);
        $participacionEstablecimientos=$operativo->establecimientosParticipaciones($cabecera);

        $coordinador=$cabecera->user()->first() ;

        return view('operativos.micabecera', ['operativo' => $operativo])
            ->with('cabecera',$cabecera)
            ->with('establecimientosnivelprimario',$establecimientosnivelprimario)
            ->with('establecimientosnivelsecundario',$establecimientosnivelsecundario)
            ->with('establecimientos_cantidad',$establecimientos->count())
            ->with('establecimientos',$establecimientos)
            ->with('seccionesnivelprimario',$seccionesNivelPrimario)
            ->with('seccionesnivelsecundario',$seccionesNivelSecundario)
            ->with('secciones_cantidad',$secciones->count())
            ->with('secciones',$secciones->get())
            ->with('participacionSecciones',$participacionSecciones)
            ->with('participacionEstablecimientos',$participacionEstablecimientos)
            ->with('coordinador',$coordinador)
            ->with('title','Cabecera');
    }

    /*Muestra los datos todas las cabeceras*/
    public static function mostrarDatosOperativo(Operativo $operativo)
    {
        /*Acá tengo que buscar las estadisticas que necesitan de cada opeartivo*/

        $establecimientos = $operativo->establecimientosDeOperativo($operativo)->get();
        $secciones=$operativo->seccionesDeOperativo($operativo);

        $establecimientosnivelprimario=$establecimientos->where('nivel_id','1')->count();
        $establecimientosnivelsecundario=$establecimientos->where('nivel_id','2')->count();

        $seccionesNivelPrimario=$operativo->seccionesDePrimariadeOperativo($operativo)->count();
        $seccionesNivelSecundario=$secciones->count()-$seccionesNivelPrimario;

        $alumnosnivelprimario=$operativo->seccionesDePrimariadeOperativo($operativo)->sum('matriculaaevaluar');
        $alumnosnivelsecundario=$secciones->sum('matriculaaevaluar')-$alumnosnivelprimario;

        $participacionSecciones=$operativo->seccionesParticipacionesenOperativo($operativo);
        $participacionEstablecimientos=$operativo->establecimientosParticipacionesenOperativo($operativo);


        $cabeceras=$operativo->cabeceras()->distinct('id')->orderBy('descripcion') ->get();

        return view('operativos.mioperativo', ['operativo' => $operativo])
            ->with('establecimientosnivelprimario',$establecimientosnivelprimario)
            ->with('establecimientosnivelsecundario',$establecimientosnivelsecundario)
            ->with('establecimientos_cantidad',$establecimientos->count())
            ->with('establecimientos',$establecimientos)
            ->with('seccionesnivelprimario',$seccionesNivelPrimario)
            ->with('seccionesnivelsecundario',$seccionesNivelSecundario)
            ->with('secciones_cantidad',$secciones->count())
            ->with('cabeceras',$cabeceras)
            ->with('participacionSecciones',$participacionSecciones)
            ->with('participacionEstablecimientos',$participacionEstablecimientos)
            ->with('alumnosnivelprimario',$alumnosnivelprimario)
            ->with('alumnosnivelsecundario',$alumnosnivelsecundario)
            ->with('title','Modificar Operativo');
    }



    public function actualizarEstablecimiento(Operativo $operativo, Cabecera $cabecera, Establecimiento $establecimiento)
    {

       $establecimientoOperativo=OperativoEstablecimiento::where('establecimiento_id',  $establecimiento->id)
           ->where('operativo_id',  $operativo->id)->first();


       $cabeceras=Cabecera::orderBy('descripcion')->pluck('descripcion', 'id');
       $participacion=Participacion::orderBy('id')->pluck('descripcion', 'id')->toArray();
       $secciones=$operativo->seccionesDeEstablecimiento($establecimiento)->get();

        return view('operativos.establecimiento', ['operativo' => $operativo])
            ->with('establecimiento',$establecimiento)
            ->with('secciones',$secciones)
            ->with('cabeceras',$cabeceras)
            ->with('participaciones',$participacion)
            ->with('cabecera',$cabecera)
            ->with('establecimientoOperativo',$establecimientoOperativo)
            ->with('title','Actualizar Establecimiento');
    }

    public function guardarActualizacionEstablecimiento(Request $request)
    {
        $data = request()->all();

        if (!isset($data['veedor']))
        {
            $data['veedor'] = '';
            $data['veedor_telefono']='';
            $data['veedor_dni']='';
            $data['veedor_mail']='';
            $data['veedor_nroempleado']='';
            $data['pagar']=false;
        }

        if (!isset($data['retiromaterialcajas']))
        {
            $data['retiromaterialcajas']=null;
        }

        $operativoEstablecimiento=OperativoEstablecimiento::find( $data['operativo_establecimiento_id']);

        $operativoEstablecimiento->update(array(
                        'codsupervisor' => $data['codsupervisor'],
                        'nombresupervisor' => $data['nombresupervisor'],
                        'director' => $data['director'],
                        'directortelefono' => $data['directortelefono'],
                        'veedor' =>$data['veedor'],
                        'veedor_telefono' => $data['veedor_telefono'],
                        'veedor_dni' => $data['veedor_dni'],
                        'veedor_mail' => $data['veedor_mail'],
                        'veedor_nroempleado' => $data['veedor_nroempleado'],
                        'pagar' => $data['pagar'],
                        'cabecera_id' => $data['cabecera'],
                        'participacion_id' => $data['participaestablecimiento'],
                        'retiromaterial'=> $data['retiromaterial'],
                        'retiromaterialcajas'=> $data['retiromaterialcajas'],
        ));

        /**Si la participacion del establecimiento es "No Participa",
         * todas sus secciones no participan, asi que hay que modificarlo de una*/

        if ( $data['participaestablecimiento']==2)
        {
            OperativoSeccion::where('operativo_id', $operativoEstablecimiento->operativo_id)
                ->where('establecimiento_id', $operativoEstablecimiento->establecimiento_id)
                ->update(array('matriculaaevaluar' => 0,
                    'matriculaevaluada' => 0,
                    'aplicador' => '',
                    'participacion_id' =>  $data['participaestablecimiento'],
                    'aplico' => false,
                    'pagar' => false));
        }
        /*Tengo que llamar a la pantalla de la cabecera que lo llame antes!*/
        //Operativo $operativo, Cabecera $cabecera
        $cabecera=Cabecera::find($data['cabecera_id']);
        $operativo=Operativo::find($data['operativo_id']);
        $establecimiento=Establecimiento::find($operativoEstablecimiento->establecimiento_id);


       // return OperativosController::mostrarDatosCabecera($operativo, $cabecera);
        return OperativosController::actualizarEstablecimiento($operativo,$cabecera, $establecimiento );
    }


    public function guardarActualizacionCabecera(Request $request)
    {
        $data = request()->all();

        $operativoCabecera=OperativoCabecera::find( $data['operativo_cabecera_id']);

        $operativoCabecera->update(array(
            'ubicacion' => $data['ubicacion'],
            'direccion' => $data['direccion'],
            'codigopostal' => $data['codigopostal'],
            'departamento_id' =>$data['departamento'],
            'localidad_id' => $data['localidad'],
        ));


        // return OperativosController::mostrarDatosCabecera($operativo, $cabecera);
        return redirect()->route('welcome');
    }

    public function actualizarSeccion(Request $request)
    {

        $seccion = OperativoSeccion::find($request->get('id'));

        $data = request()->all();
       /* if (isset( $data['aplico']) &&  $data['aplico']=='on')
            $aplico=1;
        else
            $aplico=0;

        if (isset( $data['pagar'])  &&  $data['pagar']=='on')
            $pagar=1;
        else
            $pagar=0;*/

        if (!isset($data['matriculaaevaluar']))
        {
            $data['matriculaaevaluar']=0;
            $data['matriculaevaluada']=0;
            $data['aplicador']='';
            $data['aplicador_telefono']='';
            $data['aplicador_mail']='';
            $data['aplicador_dni']='';
            $data['aplicador_nroempleado']='';
        }

        OperativoSeccion::where('id', $data['id'])
            ->update(array('matriculaaevaluar' => $data['matriculaaevaluar'],
                            'matriculaevaluada' => $data['matriculaevaluada'],
                            'aplicador' => $data['aplicador'],
                            'aplicador_telefono' => $data['aplicador_telefono'],
                            'aplicador_mail' => $data['aplicador_mail'],
                            'aplicador_dni' => $data['aplicador_dni'],
                            'aplicador_nroempleado' => $data['aplicador_nroempleado'],
                            'participacion_id' => $data['participaseccion'],
                            'aplico' => $data['aplico'],
                            'pagar' => $data['pagar']));



        $cabecera=Cabecera::find($seccion->cabecera_id);
        $operativo=Operativo::find($seccion->operativo_id);
        $establecimiento=Establecimiento::find($seccion->establecimiento_id);


        // return OperativosController::mostrarDatosCabecera($operativo, $cabecera);
        return OperativosController::actualizarEstablecimiento($operativo,$cabecera, $establecimiento );
    }

    public function update(Operativo $operativo)
    {
        /*$data = request()->validate([
            'name' => 'required',
            'email' => ['required', 'email', Rule::unique('users')->ignore($user->id)],
            'password' => '',
        ]);*/

        $data = request()->all();

        $operativo->update($data);
        return redirect()->route('operativos', ['operativo' => $operativo]);
    }

    function destroy(Operativo $operativo)
    {
        $operativo->delete();
        return redirect()->route('operativos');
    }


    public static function ExportarOperativoEstablecimientos(Operativo $operativo, Cabecera $cabecera )
    {
        return (new OperativoEstablecimientosExport($operativo, $cabecera))->download($operativo->nombre.'_MiCabecera_Establecimientos.xlsx');
    }

    public static function ExportarOperativoEstablecimientosAdmin(Operativo $operativo)
    {
        return (new OperativoEstablecimientosAdminExport($operativo))->download($operativo->nombre.'_Establecimientos.xlsx');
    }

    public static function ExportarOperativoVeedores(Operativo $operativo, Cabecera $cabecera )
    {
        return (new OperativoVeedoresExport($operativo, $cabecera))->download($operativo->nombre.'_MiCabecera_Veedores.xlsx');
    }

    public static function ExportarOperativoVeedoresAdmin(Operativo $operativo )
    {
        return (new OperativoVeedoresAdminExport($operativo))->download($operativo->nombre.'_Veedores.xlsx');
    }

    public static function ExportarOperativoAplicadores(Operativo $operativo, Cabecera $cabecera )
    {
        return (new OperativoAplicadoresExport($operativo, $cabecera))->download($operativo->nombre.'_MiCabecera_Aplicadores.xlsx');
    }

    public static function ExportarOperativoAplicadoresAdmin(Operativo $operativo)
    {
        return (new OperativoAplicadoresAdminExport($operativo))->download($operativo->nombre.'_MiCabecera_Aplicadores.xlsx');
    }

    public static function ExportarOperativoParticipacion(Operativo $operativo, Cabecera $cabecera )
    {
        return (new OperativoParticipacionExport($operativo, $cabecera))->download($operativo->nombre.'_MiCabecera_Participacion.xlsx');
    }

    public static function ExportarOperativoParticipacionAdmin(Operativo $operativo)
    {
        return (new OperativoParticipacionAdminExport($operativo))->download($operativo->nombre.'_Participacion.xlsx');
    }

    public static function ExportarOperativoEstadisticasParticipacionAdmin(Operativo $operativo)
    {
        return (new OperativoEstadisticasParticipacionAdminExport($operativo))->download($operativo->nombre.'_EstadisticasParticipacion.xlsx');
    }

    public static function ExportarOperativoCabeceras(Operativo $operativo)
    {
        return (new OperativoCabeceraExport($operativo))->download($operativo->nombre.'_Cabeceras.xlsx');
    }

    public static function ExportarOperativoPagos(Operativo $operativo)
    {
        return (new OperativoPagosExport($operativo))->download($operativo->nombre . '_Pagos.xlsx');
    }


}
