<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PeriodoFuncionamiento;

class PeriodosFuncionamientoController extends Controller
{

    public function index()
    {
        $data =  PeriodoFuncionamiento::all();
        return view('periodosfuncionamiento.index')->with('periodosfuncionamiento', $data)->with('title','Periodos de Funcionamiento');
    }

    public function create()
    {
        return view('periodosfuncionamiento.create')->with('title','Crear un Nuevo Período de Funcionamiento');
    }

    public function store()
    {
        /*$data=request().validate(
            ['descripcion'=> 'required']
            );*/
        $data = request()->all();


        PeriodoFuncionamiento::create(['descripcion'=>$data['descripcion']]);
        return redirect()->route('periodosfuncionamiento');
    }

    public function edit(PeriodoFuncionamiento $periodofuncionamiento)
    {
        return view('periodosfuncionamiento.edit', ['periodofuncionamiento' => $periodofuncionamiento])->with('title','Modificar Período de Funcionamiento');
    }

    public function update(PeriodoFuncionamiento $periodofuncionamiento)
    {
        /*$data = request()->validate([
            'name' => 'required',
            'email' => ['required', 'email', Rule::unique('users')->ignore($user->id)],
            'password' => '',
        ]);*/

        $data = request()->all();

        $periodofuncionamiento->update($data);
        return redirect()->route('periodosfuncionamiento', ['periodofuncionamiento' => $periodofuncionamiento]);

    }

    function destroy(PeriodoFuncionamiento $periodofuncionamiento)
    {
        $periodofuncionamiento->delete();
        return redirect()->route('periodosfuncionamiento');
    }


}
