<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sector;

class SectoresController extends Controller
{

    public function index()
    {
        $data =  Sector::all();
        return view('sectores.index')->with('sectores', $data)->with('title','Sectores');
    }

    public function create()
    {
        return view('sectores.create')->with('title','Crear un Nuevo Sector');
    }

    public function store()
    {
        /*$data=request().validate(
            ['descripcion'=> 'required']
            );*/
        $data = request()->all();


        Sector::create(['descripcion'=>$data['descripcion']]);
        return redirect()->route('sectores');
    }

    public function edit(Sector $sector)
    {
        return view('sectores.edit', ['sector' => $sector])->with('title','Modificar Sector');
    }

    public function update(Sector $sector)
    {
        /*$data = request()->validate([
            'name' => 'required',
            'email' => ['required', 'email', Rule::unique('users')->ignore($user->id)],
            'password' => '',
        ]);*/

        $data = request()->all();

        $sector->update($data);
        return redirect()->route('sectores', ['sector' => $sector]);

    }

    function destroy(Sector $sector)
    {
        $sector->delete();
        return redirect()->route('sectores');
    }


}
