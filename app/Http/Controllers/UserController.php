<?php

namespace App\Http\Controllers;
use App\Models\Cabecera;
use App\Models\Departamento;
use App\Models\Localidad;
use App\Models\Operativo;
use App\Models\OperativoCabecera;
use App\Models\OperativoEstablecimiento;
use App\User;
use Illuminate\Http\Request;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
   public function index(Request $request)
    {

        /*obtengo los parametros por get que eligieron en la busqueda.*/
        $nombre=$request->get('nombre');
        $apellido=$request->get('apellido');
        $dni=$request->get('dni');
        $cabecera=$request->get('cabecera');

        $cabeceras=Cabecera::orderBy('descripcion')->pluck('descripcion', 'id')->toArray();


        $data =  User::nombre($nombre)
            ->apellido($apellido)
            ->dni($dni)
            ->orderBy('nombre', 'ASC')->paginate();

        return view('users.index')
            ->with('users', $data)
            ->with('cabeceras', $cabeceras)
            ->with('title','Usuarios');
    }

    public function create()
    {
        $cabeceras=Cabecera::orderBy('descripcion')->pluck('descripcion', 'id')->toArray();

        return view('users.create')
            ->with('cabeceras', $cabeceras)
            ->with('title','Crear un Nuevo Usuario');
    }

    public function store()
    {
        /*$data=request().validate(
            ['descripcion'=> 'required']
            );*/
        $data = request()->all();

        if (!isset($data['cabecera']))
            $data['cabecera']="";

        $user=User::create(['login'=>$data['login'],
            'password'=>bcrypt($data['login']),
            'esAdmin'=>$data['esAdmin'],
            'email'=>$data['email'],
            'apellido'=>$data['apellido'],
            'nombre'=>$data['nombre'],
            'dni'=>$data['dni'],
            'direccion'=>$data['direccion'],
            'cbu'=>$data['cbu'],
            'cuit'=>$data['cuit'],
            'telefono'=>$data['telefono'],
            'numeroempleado'=>$data['numeroempleado']]);

        /*Y si eligieron una cabecera, entonces asignar ese usuario a la cabecera*/

        if (isset($data['cabecera']) && $data['cabecera'] !="")
        {
            $operativoactual=Operativo::where('actual','1')->first();

            Cabecera::where('id', $data['cabecera'])->update(array('user_id' => $user->id));

            OperativoCabecera::where('cabecera_id', $data['cabecera'])
                ->where('operativo_id',$operativoactual->id)
                ->update(array('user_id' => $user->id));

            OperativoEstablecimiento::where('cabecera_id', $data['cabecera'])
                ->where('operativo_id',$operativoactual->id)
                ->update(array('user_id' => $user->id));

        }

        return redirect()->route('users');
    }

    public function edit(User $user)
    {
        $cabeceras=Cabecera::orderBy('descripcion')->pluck('descripcion', 'id')->toArray();

        if ($user->cabecera!=null)
            $cabecera=$user->cabecera->id;
        else
            $cabecera=null;

        return view('users.edit', ['user' => $user])
            ->with('title','Modificar Usuario')
            ->with('cabeceras',$cabeceras)
            ->with('cabecera',$cabecera);
    }

    public function update(User $user)
    {
        /*$data = request()->validate([
            'name' => 'required',
            'email' => ['required', 'email', Rule::unique('users')->ignore($user->id)],
            'password' => '',
        ]);*/

        $data = request()->all();

        $user->update($data);

        if (isset($data['cabecera']) && $data['cabecera'] !="")
        {
            Cabecera::where('id', $data['cabecera'])->update(array('user_id' => $user->id));

            $operativoactual=Operativo::where('actual','1')->first();

            OperativoCabecera::where('cabecera_id', $data['cabecera'])
                ->where('operativo_id',$operativoactual->id)
                ->update(array('user_id' => $user->id));

            OperativoEstablecimiento::where('cabecera_id', $data['cabecera'])
                ->where('operativo_id',$operativoactual->id)
                ->update(array('user_id' => $user->id));
        }


        return redirect()->route('users', ['user' => $user]);

    }

    public function resetpassword(User $user)
    {

        $user->password = bcrypt($user->login);
        $user->save();

        return redirect()->route('users')->with("success","Contraseña reseatada !");

    }

    public function cambiarpassword()
    {
        $user=auth()->user();

        return view('users.cambiarpassword', ['user' => $user])
            ->with('title','Modificar Usuario');
    }

    public function guardarCambioPassword(Request $request)
    {

        $data = request()->all();

        if(strcmp($data['contraseniaconfirmada'], $data['contrasenia']) != 0){
            //Current password and new password are same
            return redirect()->back()->with("error","Las claves no coinciden. Verifique");
        }

        //Change Password
        $user =auth()->user();
        $user->password = bcrypt($request->get('contrasenia'));
        $user->save();

        return redirect()->back()->with("success","Contraseña Modificada !");

    }

    public function welcome()
    {

        /*Acá tengo que buscar las estadisticas que necesitan de cada opeartivo*/
        /*Si es un usuario ADMIN tengo que mostrar todos los operativos
        Sino, solo el que está activo*/


        $operativos=Operativo::all();
        $operativoactual=Operativo::where('actual',1)->first();
        $idOperativoActual=$operativoactual->id;


        if (auth()->user()->esAdmin)
        {

            return view('welcome', ['operativo' => $operativoactual])
                ->with('operativos', $operativos)
                ->with('title','Estadísticas / Listados');
        }

        else
        {

            if (auth()->user()->cabecera()->first() ==null)
                $cabeceraDatos=null;
            else
                $cabeceraDatos=OperativoCabecera::where('cabecera_id', auth()->user()->cabecera()->first()->id)
                    ->where('operativo_id',$idOperativoActual)->get();

            $localidades = Localidad::orderBy('descripcion')->pluck('descripcion', 'id')->toArray();
            $departamentos=Departamento::orderBy('descripcion')->pluck('descripcion', 'id')->toArray();

            return view('welcomecoordinador', ['operativocabecera' => $cabeceraDatos->first()])
                ->with('operativo', $operativoactual )
                ->with('cabecera', auth()->user()->cabecera()->first())
                ->with('localidades', $localidades)
                ->with('departamentos', $departamentos)
                ->with('title','Mi cabecera');
        }

    }


    function destroy(User $user)
    {
        $user->delete();
        return redirect()->route('users');
    }

    public function export()
    {
        return Excel::download(new UsersExport, 'users.xlsx');
    }
}
