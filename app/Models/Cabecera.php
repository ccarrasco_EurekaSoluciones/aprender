<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cabecera extends Model
{
    protected $fillable = array('descripcion','localidad', 'user_id');

    public function establecimientos()
    {
        return $this->hasMany('App\Models\Establecimiento');
    }


    public function user()
    {
        return $this->belongsTo('App\User');
    }



}
