<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Establecimiento extends Model
{
    protected $table = 'establecimientos';

    protected $fillable = array('cue',
        'nombre',
        'localidad_id',
        'departamento_id',
        'domicilio',
        'telefono',
        'email',
        'sector_id',
        'ambito_id',
        'periodofuncionamiento_id',
        'nivel_id',
        'modalidad_id',
        'distrito',
        'codsupervisor',
        'nombresupervisor',
        'cabecera_id');

    public function nivel()
    {
        return $this->belongsTo('App\Models\Nivel');
    }

    public function localidad()
    {
        return $this->belongsTo('App\Models\Localidad');
    }

    public function sector()
    {
        return $this->belongsTo('App\Models\Sector');
    }

    public function ambito()
    {
        return $this->belongsTo('App\Models\Ambito');
    }

    public function modalidad()
    {
        return $this->belongsTo('App\Models\Modalidad');
    }

    public function periodofuncionamiento()
    {
        return $this->belongsTo('App\Models\PeriodoFuncionamiento');
    }

    public function departamento()
    {
        return $this->belongsTo('App\Models\Departamento');
    }


    public function cabecera()
    {
        return $this->belongsTo('App\Models\Cabecera');
    }

    public function operativos()
    {
        return $this->belongsToMany('App\Models\Operativo')->withTimestamps();
    }

    public function scopeNombre($query, $nombre)
    {
        if (trim($nombre!=""))
            $query->where('nombre', 'like', "%$nombre%");
    }
    public function scopeCue($query, $cue)
    {
        if (trim($cue!=""))
            $query->where('cue', 'like', "%$cue%");
    }

    public function scopeLocalidad($query, $localidad)
    {
        if (trim($localidad!=""))
            $query->where('localidad_id', $localidad);
    }

    public function scopeNivel($query, $nivel)
    {
        if (trim($nivel!=""))
            $query->where('nivel_id', $nivel);
    }

    public function scopeSector($query, $sector)
    {
        if (trim($sector!=""))
            $query->where('sector_id', $sector);
    }

    public function scopeAmbito($query, $ambito)
    {
        if (trim($ambito!=""))
            $query->where('ambito_id', $ambito);
    }

    public function scopeModalidad($query, $modalidad)
    {
        if (trim($modalidad!=""))
            $query->where('modalidad_id', $modalidad);
    }

    public function scopeDepartamento($query, $departamento)
    {
        if (trim($departamento!=""))
            $query->where('departamento_id', $departamento);
    }

    public function scopePeriodofuncionamiento($query, $periodo)
    {
        if (trim($periodo!=""))
            $query->where('periodofuncionamiento_id', $periodo);
    }

    public function scopeCabecera($query, $cabecera)
    {
        if (trim($cabecera!=""))
            $query->where('cabecera_id', $cabecera);
    }

}
