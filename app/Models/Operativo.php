<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Operativo extends Model
{
    protected $table = 'operativos';
    protected $fillable = array('nombre','grado','anio','materiasgrado','materiasanio','fecha');


    public function cabeceras()
    {
        return
            $this->belongsToMany
            ('App\Models\Cabecera',
                'operativos_establecimientos',
                'operativo_id', 'cabecera_id'

            );
    }

    public function cabecerascondatos()
    {
        return
            $this->belongsToMany
            ('App\Models\Cabecera',
                'operativos_cabeceras',
                'operativo_id', 'cabecera_id'

            )->withPivot('ubicacion','direccion', 'codigopostal',
                'departamento_id', 'localidad_id','user_id');
    }


    public function establecimientos()
    {
        return
            $this->belongsToMany
            ('App\Models\Establecimiento',
                'operativos_establecimientos',
                'operativo_id', 'establecimiento_id'

            )->withPivot('cabecera_id', 'participacion_id', 'director',
                'directortelefono','veedor','veedor_dni','veedor_telefono',
                'veedor_mail','veedor_nroempleado', 'pagar', 'codsupervisor',
                'nombresupervisor','retiromaterial','retiromaterialcajas');
    }

    public function establecimientosDeCabecera($cabecera)
    {
        return
            $this->belongsToMany
            ('App\Models\Establecimiento',
                'operativos_establecimientos',
                'operativo_id', 'establecimiento_id'
            )->withPivot('cabecera_id', 'participacion_id', 'director',
                'directortelefono','veedor', 'codsupervisor', 'nombresupervisor',
                'veedor_dni','veedor_telefono','veedor_mail','veedor_nroempleado',
                'pagar','retiromaterial','retiromaterialcajas')
           ->where('operativos_establecimientos.cabecera_id',$cabecera['id']);

                /*->orderBy('nombre')->get();*/

    }

    public function establecimientosDeOperativo($operativo)
    {
        return
            $this->belongsToMany
            ('App\Models\Establecimiento',
                'operativos_establecimientos',
                'operativo_id', 'establecimiento_id'
            )->withPivot('cabecera_id', 'participacion_id', 'director',
                'directortelefono','veedor', 'codsupervisor', 'nombresupervisor',
                'veedor_dni','veedor_telefono','veedor_mail','veedor_nroempleado' ,
                'pagar','retiromaterial','retiromaterialcajas')
                ->where('operativos_establecimientos.operativo_id',$operativo['id']);

        /*->orderBy('nombre')->get();*/

    }

    public function establecimientosDeOperativoyCabecera($operativo, $cabecera)
    {
        return
            $this->belongsToMany
            ('App\Models\Establecimiento',
                'operativos_establecimientos',
                'operativo_id', 'establecimiento_id'
            )->withPivot('cabecera_id', 'participacion_id', 'director',
                'directortelefono','veedor', 'codsupervisor', 'nombresupervisor',
                'veedor_dni','veedor_telefono','veedor_mail','veedor_nroempleado',
                'pagar','retiromaterial','retiromaterialcajas')
                ->where('operativos_establecimientos.operativo_id',$operativo['id'])
                ->where('operativos_establecimientos.cabecera_id',$cabecera['id']);

        /*->orderBy('nombre')->get();*/

    }

    public function  establecimientosParticipaciones($cabecera)
    {

        $participacion=array();
        $participacion["SI"]=$this->establecimientosDeCabecera($cabecera)->wherePivot('participacion_id',"1")->count();
        $participacion["NO"]=$this->establecimientosDeCabecera($cabecera)->wherePivot('participacion_id',"2")->count();
        $participacion["SD"]=$this->establecimientosDeCabecera($cabecera)->wherePivot('participacion_id',"3")->count();
        $participacion["NC"]=$this->establecimientosDeCabecera($cabecera)->wherePivot('participacion_id',"4")->count();

        $participacion["veedorSI"]=$this->establecimientosDeCabecera($cabecera)
                    ->wherePivot('participacion_id',"1")
                    ->wherePivot('veedor',"<>","")
                    ->count();
        $participacion["veedorNO"]=$this->establecimientosDeCabecera($cabecera)
            ->wherePivot('participacion_id',"1")
            ->wherePivot('veedor',"=","")
            ->count();

        return $participacion;

    }

    public function  establecimientosParticipacionesenOperativo($operativo)
    {

        $participacion=array();
        $participacion["SI"]=$this->establecimientosDeOperativo($operativo)->wherePivot('participacion_id',"1")->count();
        $participacion["NO"]=$this->establecimientosDeOperativo($operativo)->wherePivot('participacion_id',"2")->count();
        $participacion["SD"]=$this->establecimientosDeOperativo($operativo)->wherePivot('participacion_id',"3")->count();
        $participacion["NC"]=$this->establecimientosDeOperativo($operativo)->wherePivot('participacion_id',"4")->count();

        $participacion["veedorSI"]=$this->establecimientosDeOperativo($operativo)
            ->wherePivot('participacion_id',"1")
            ->wherePivot('veedor',"<>","")
            ->count();
        $participacion["veedorNO"]=$this->establecimientosDeOperativo($operativo)
            ->wherePivot('participacion_id',"1")
            ->count()-$participacion["veedorSI"];

        return $participacion;

    }


    public function secciones()
    {
        return
            $this->belongsToMany
            ('App\Models\Establecimiento',
                'operativos_secciones',
                'operativo_id', 'establecimiento_id'

            )->withPivot('cabecera_id','gradooanio', 'seccion', 'turno',
                'codigoplan','nivel', 'matricula', 'matriculaaevaluar',
                'matriculaevaluada', 'aplicador', 'aplicador_dni',
                'aplicador_telefono','aplicador_nroempleado','aplicador_mail',
                'aplico','pagar', 'participacion_id');
    }

    public function  seccionesDeCabecera($cabecera)
    {
        return
            $this->belongsToMany
            ('App\Models\Establecimiento',
                'operativos_secciones',
                'operativo_id', 'establecimiento_id'

            )->withPivot('cabecera_id','gradooanio', 'seccion', 'turno',
                'codigoplan','nivel', 'matricula', 'matriculaaevaluar',
                'matriculaevaluada', 'aplicador', 'aplicador_dni',
                'aplicador_telefono','aplicador_nroempleado','aplicador_mail',
                'aplico','pagar','participacion_id')->where('operativos_secciones.cabecera_id',$cabecera['id']);

    }

    public function  seccionesDeOperativo($operativo)
    {
        return
            $this->belongsToMany
            ('App\Models\Establecimiento',
                'operativos_secciones',
                'operativo_id', 'establecimiento_id'

            )->withPivot('cabecera_id','gradooanio', 'seccion', 'turno',
                'codigoplan','nivel', 'matricula', 'matriculaaevaluar',
                'matriculaevaluada', 'aplicador','aplicador_dni','aplicador_telefono',
                'aplicador_mail','aplicador_nroempleado',
                'aplico','pagar','participacion_id')->where('operativos_secciones.operativo_id',$operativo['id']);


    }

    public function  seccionesDeEstablecimiento($establecimiento)
    {
        return
            $this->belongsToMany
            ('App\Models\Establecimiento',
                'operativos_secciones',
                'operativo_id', 'establecimiento_id'

            )->withPivot('cabecera_id','gradooanio', 'seccion', 'turno',
                'codigoplan','nivel', 'matricula', 'matriculaaevaluar',
                'matriculaevaluada', 'aplicador', 'aplicador_dni','aplicador_telefono',
                'aplicador_mail','aplicador_nroempleado',
                'aplico','pagar','participacion_id','id')
                ->where('operativos_secciones.establecimiento_id',$establecimiento['id']);
    }

    public function  seccionesDeEstablecimientoporid($establecimiento)
    {
        return
            $this->belongsToMany
            ('App\Models\Establecimiento',
                'operativos_secciones',
                'operativo_id', 'establecimiento_id'

            )->withPivot('cabecera_id','gradooanio', 'seccion', 'turno',
                'codigoplan','nivel', 'matricula', 'matriculaaevaluar',
                'matriculaevaluada', 'aplicador', 'aplicador_dni','aplicador_telefono',
                'aplicador_mail','aplicador_nroempleado',
                'aplico','pagar','participacion_id','id')
                ->where('operativos_secciones.establecimiento_id',$establecimiento);
    }

    public function  seccionesDePrimaria($cabecera)
    {
        return
            $this->belongsToMany
            ('App\Models\Establecimiento',
                'operativos_secciones',
                'operativo_id', 'establecimiento_id'

            )->withPivot('cabecera_id','gradooanio', 'seccion', 'turno',
                'codigoplan','nivel', 'matricula', 'matriculaaevaluar',
                'matriculaevaluada', 'aplicador','aplicador_dni','aplicador_telefono',
                'aplicador_mail','aplicador_nroempleado',
                'aplico','pagar', 'participacion_id')->where('operativos_secciones.cabecera_id',$cabecera['id'])
                ->wherePivot('nivel',"Primario");
    }

    public function  seccionesDePrimariadeOperativo($operativo)
    {
        return
            $this->belongsToMany
            ('App\Models\Establecimiento',
                'operativos_secciones',
                'operativo_id', 'establecimiento_id'

            )->withPivot('cabecera_id','gradooanio', 'seccion', 'turno',
                'codigoplan','nivel', 'matricula', 'matriculaaevaluar',
                'matriculaevaluada', 'aplicador', 'aplicador_dni','aplicador_telefono',
                'aplicador_mail','aplicador_nroempleado',
                'aplico','pagar', 'participacion_id')->where('operativos_secciones.operativo_id',$operativo['id'])
                ->wherePivot('nivel',"Primario");
    }

    public function  seccionesDeSecundaria($cabecera)
    {
        return
            $this->belongsToMany
            ('App\Models\Establecimiento',
                'operativos_secciones',
                'operativo_id', 'establecimiento_id'

            )->withPivot('cabecera_id','gradooanio', 'seccion', 'turno',
                'codigoplan','nivel', 'matricula', 'matriculaaevaluar',
                'matriculaevaluada', 'aplicador','aplicador_dni','aplicador_telefono',
                'aplicador_mail','aplicador_nroempleado',
                'aplico','pagar','participacion_id')->where('operativos_secciones.cabecera_id',$cabecera['id'])
                ->wherePivot('nivel', "<>","Primario");
    }
    public function  seccionesParticipaciones($cabecera)
    {
        $participacion=array();
        $participacion["SI"]=$this->seccionesDeCabecera($cabecera)->wherePivot('participacion_id',"1")->count();
        $participacion["NO"]=$this->seccionesDeCabecera($cabecera)->wherePivot('participacion_id',"2")->count();
        $participacion["SD"]=$this->seccionesDeCabecera($cabecera)->wherePivot('participacion_id',"3")->count();
        $participacion["NC"]=$this->seccionesDeCabecera($cabecera)->wherePivot('participacion_id',"4")->count();

        $participacion["aplicadorSI"]=$this->seccionesDeCabecera($cabecera)
            ->wherePivot('participacion_id',"1")
            ->wherePivot('aplicador',"<>", "")
            ->count();

        $participacion["aplicadorNO"]=$this->seccionesDeCabecera($cabecera)
            ->wherePivot('participacion_id',"1")
            ->count() - $participacion["aplicadorSI"];

        $participacion["matricula"]=$this->seccionesDeCabecera($cabecera)
            ->sum('matricula');
        //->wherePivot('participacion_id',"1")


        $participacion["matriculaaevaluar"]=$this->seccionesDeCabecera($cabecera)
            ->wherePivot('participacion_id',"1")
            ->sum('matriculaaevaluar');

        $participacion["matriculaevaluada"]=$this->seccionesDeCabecera($cabecera)
            ->wherePivot('participacion_id',"1")
            ->sum('matriculaevaluada');

        return $participacion;

    }

    public function  seccionesParticipacionesenOperativo($operativo)
    {
        $participacion=array();
        $participacion["SI"]=$this->seccionesDeOperativo($operativo)->wherePivot('participacion_id',"1")->count();
        $participacion["NO"]=$this->seccionesDeOperativo($operativo)->wherePivot('participacion_id',"2")->count();
        $participacion["SD"]=$this->seccionesDeOperativo($operativo)->wherePivot('participacion_id',"3")->count();
        $participacion["NC"]=$this->seccionesDeOperativo($operativo)->wherePivot('participacion_id',"4")->count();

        $participacion["aplicadorSI"]=$this->seccionesDeOperativo($operativo)
            ->wherePivot('participacion_id',"1")
            ->wherePivot('aplicador',"<>", "")
            ->count();

        $participacion["aplicadorNO"]=$this->seccionesDeOperativo($operativo)
            ->wherePivot('participacion_id',"1")
            ->wherePivot('aplicador',"=", "")
            ->count();

        $participacion["matriculaTotal"]=$this->seccionesDeOperativo($operativo)
            ->sum('matriculaaevaluar');
        $participacion["matriculaSI"]=$this->seccionesDeOperativo($operativo)
            ->wherePivot('participacion_id',"1")
            ->sum('matriculaaevaluar');
        return $participacion;

    }



}
