<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OperativoCabecera extends Model
{
    protected $table = 'operativos_cabeceras';

    protected $fillable = array('operativo_id','cabecera_id','ubicacion',
        'direccion','codigopostal','departamento_id','localidad_id', 'user_id');

    public function departamento()
    {
        return $this->belongsTo('App\Models\Departamento');
    }

    public function localidad()
    {
        return $this->belongsTo('App\Models\Localidad');
    }
}
