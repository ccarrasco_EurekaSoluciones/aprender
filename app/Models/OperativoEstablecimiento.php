<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OperativoEstablecimiento extends Model
{
    protected $table = 'operativos_establecimientos';

    protected $fillable = array('operativo_id','establecimiento_id','cabecera_id','participacion_id',
        'director','directortelefono','veedor', 'veedor_dni','veedor_telefono',
        'veedor_mail','veedor_nroempleado', 'pagar', 'codsupervisor', 'nombresupervisor',
        'user_id', 'retiromaterial', 'retiromaterialcajas');

}