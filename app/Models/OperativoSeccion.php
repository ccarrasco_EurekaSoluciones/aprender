<?php
/**
 * Created by PhpStorm.
 * User: ccarrasco
 * Date: 18/07/2018
 * Time: 01:54 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OperativoSeccion extends  Model
{
    protected $table = 'operativos_secciones';
    protected $fillable = array('operativo_id','cabecera_id','establecimiento_id','gradooanio','seccion','turno',
        'matricula','codigoplan','nivel','matriculaaevaluar','matriculaevaluada', 'aplicador',
        'aplico', 'pagar', 'participacion_id');



}