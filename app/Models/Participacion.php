<?php
/**
 * Created by PhpStorm.
 * User: ccarrasco
 * Date: 18/07/2018
 * Time: 09:29 AM
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Participacion extends Model
{
    protected $table = 'participacion';
    protected $fillable = array('descripcion');
}