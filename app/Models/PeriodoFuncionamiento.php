<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PeriodoFuncionamiento extends Model
{
    protected $table = 'periodosfuncionamiento';
    protected $fillable = array('descripcion');
}
