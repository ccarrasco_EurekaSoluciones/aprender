<?php

namespace App;

use App\Models\Operativo;
use App\Models\OperativoEstablecimiento;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'login', 'email', 'password','apellido','nombre',
        'direccion','cbu','cuit','telefono','numeroempleado', 'esAdmin', 'dni'

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    protected $casts=[
      'admin' => 'boolesan'
    ];

    public function cabecera()
    {
        return $this->hasOne('App\Models\Cabecera');
    }

    public function scopeNombre($query, $nombre)
    {
        if (trim($nombre!=""))
            $query->where('nombre', 'like', "%$nombre%");
    }
    public function scopeApellido($query, $apellido)
    {
        if (trim($apellido!=""))
            $query->where('apellido', 'like', "%$apellido%");
    }

    public function scopeDni($query, $dni)
    {
        if (trim($dni!=""))
            $query->where('apellido', 'like', "%$dni%");
    }


    public function getNombreAndApellidoAttribute()
    {
        return $this->nombre . ' ' . $this->apellido;
    }

    public function admin()
    {
      return $this->esAdmin;
    }

    public function operativos()
    {
        if ($this->admin())
        {
            return Operativo:: all();
        }
        else
        {

            return
                $this->belongsToMany
                ('App\Models\Operativo',
                    'operativos_establecimientos',
                    'user_id', 'operativo_id'
                )->where('actual','1')->distinct()->get();
        }

    }


}
