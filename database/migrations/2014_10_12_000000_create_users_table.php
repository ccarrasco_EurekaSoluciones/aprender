<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('login',50);
            $table->string('password',128);
            $table->boolean('esAdmin');
            $table->string('email',100);
            $table->string('apellido',50);
            $table->string('nombre',50);
            $table->string('dni',50)->nullable();
            $table->string('direccion',50, null)->nullable();
            $table->string('cbu',50, null)->nullable();
            $table->string('cuit',50, null)->nullable();
            $table->string('telefono',50, null)->nullable();
            $table->string('numeroempleado',50, null)->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
