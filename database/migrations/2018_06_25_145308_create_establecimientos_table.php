<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstablecimientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('establecimientos', function (Blueprint $table) {
            $table->increments('id');
            $table->string ('cue');
            $table->string ('nombre');
            $table->unsignedInteger('localidad_id');
            $table->foreign('localidad_id')->references('id')->on('localidades');
            $table->unsignedInteger('departamento_id');
            $table->foreign('departamento_id')->references('id')->on('departamentos');
            $table->string ('domicilio', 100);
            $table->string ('telefono', 50)->nullable();
            $table->string ('email', 100)->nullable();
            $table->unsignedInteger('sector_id');
            $table->foreign('sector_id')->references('id')->on('sectores');
            $table->unsignedInteger('ambito_id');
            $table->foreign('ambito_id')->references('id')->on('ambitos');
            $table->unsignedInteger('periodofuncionamiento_id');
            $table->foreign('periodofuncionamiento_id')->references('id')->on('periodosfuncionamiento');
            $table->unsignedInteger('nivel_id');
            $table->foreign('nivel_id')->references('id')->on('niveles');
            $table->unsignedInteger('modalidad_id');
            $table->foreign('modalidad_id')->references('id')->on('modalidades');
            $table->unsignedInteger('distrito');
            $table->string('codsupervisor');
            $table->string('nombresupervisor');
            $table->unsignedInteger('cabecera_id');
            $table->foreign('cabecera_id')->references('id')->on('cabeceras');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('establecimientos');
    }
}
