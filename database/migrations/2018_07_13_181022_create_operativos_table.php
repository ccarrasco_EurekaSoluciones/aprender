<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperativosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operativos', function (Blueprint $table) {
            $table->increments('id');
            $table->string ('nombre',50);
            $table->string ('grado',10);
            $table->string ('anio',10);
            $table->string ('materiasgrado',50);
            $table->string ('materiasanio',50);
            $table->dateTime('fecha');
            $table->boolean('actual');
            $table->boolean('cerrado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operativos');
    }
}


