<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperativosEstablecimientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operativos_establecimientos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('operativo_id');
            $table->foreign('operativo_id')->references('id')->on('operativos');
            $table->unsignedInteger('establecimiento_id');
            $table->foreign('establecimiento_id')->references('id')->on('establecimientos');
            $table->unsignedInteger('cabecera_id');
            $table->foreign('cabecera_id')->references('id')->on('cabeceras');
            $table->unsignedInteger('participacion_id');
           // $table->foreign('participacion_id')->references('id')->on('participacion');
            $table->string('director',50)->nullable($value = true);
            $table->string('directortelefono',50, null)->nullable($value = true);
            $table->string('veedor',50,null)->nullable($value = true);
            $table->string('veedor_telefono',50,null)->nullable($value = true);
            $table->string('veedor_mail',100,null)->nullable($value = true);
            $table->string('veedor_nroempleado',50,null)->nullable($value = true);
            $table->string('veedor_dni',50,null)->nullable($value = true);
            $table->string('codsupervisor',50, null)->nullable($value = true);
            $table->string('nombresupervisor',50,null)->nullable($value = true);
            $table->unsignedInteger('user_id')->nullable($value = true);
            $table->foreign('user_id')->references('id')->on('users')->nullable($value = true);
            $table->boolean('pagar');
            $table->boolean('retiromaterial');
            $table->unsignedInteger('retiromaterialcajas')->nullable($value = true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operativos_establecimientos');
    }
}
