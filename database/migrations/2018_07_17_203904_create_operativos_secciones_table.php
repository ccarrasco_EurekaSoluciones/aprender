<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperativosSeccionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operativos_secciones', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('operativo_id');
            $table->foreign('operativo_id')->references('id')->on('operativos');
            $table->unsignedInteger('establecimiento_id');
            $table->foreign('establecimiento_id')->references('id')->on('establecimientos');
            $table->unsignedInteger('cabecera_id')->nullable($value = true);
            $table->foreign('cabecera_id')->references('id')->on('cabeceras');
            $table->unsignedInteger('participacion_id');
            $table->foreign('participacion_id')->references('id')->on('participacion');
            $table->unsignedInteger('gradooanio');
            $table->string('seccion',10);
            $table->string('turno',10);
            $table->string('codigoplan',10)->nullable($value = true);
            $table->string('nivel',10);
            $table->unsignedInteger('matricula');
            $table->unsignedInteger('matriculaaevaluar')->nullable($value = true);
            $table->unsignedInteger('matriculaevaluada')->nullable($value = true);
            $table->string('aplicador',50, null)->nullable($value = true);
            $table->string('aplicador_telefono',50, null)->nullable($value = true);
            $table->string('aplicador_mail',100, null)->nullable($value = true);
            $table->string('aplicador_nroempleado',50, null)->nullable($value = true);
            $table->string('aplicador_dni',50, null)->nullable($value = true);
            $table->boolean('aplico');
            $table->boolean('pagar');

            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operativos_secciones');
    }
}
