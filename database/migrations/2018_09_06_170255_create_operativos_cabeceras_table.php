<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperativosCabecerasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operativos_cabeceras', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('operativo_id');
            $table->foreign('operativo_id')->references('id')->on('operativos');
            $table->unsignedInteger('cabecera_id');
            $table->foreign('cabecera_id')->references('id')->on('cabeceras');
            $table->string('ubicacion',50)->nullable($value = true);
            $table->string('direccion',50, null)->nullable($value = true);
            $table->string('codigopostal',50,null)->nullable($value = true);
            $table->unsignedInteger('departamento_id')->nullable($value = true);
            $table->foreign('departamento_id')->references('id')->on('departamentos');
            $table->unsignedInteger('localidad_id')->nullable($value = true);
            $table->foreign('localidad_id')->references('id')->on('localidades');
            $table->unsignedInteger('user_id')->nullable($value = true);
            $table->foreign('user_id')->references('id')->on('users')->nullable($value = true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operativos_cabeceras');
    }

}
