<?php
use App\Models;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades;
class AmbitoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $valores=['Urbano',
            'Rural Aglomerado',
            'Rural Disperso',
        ];

        foreach ($valores as $valor)
        {

            Models\Ambito::create([
                'descripcion'=>$valor,
            ]);
        }

    }
}
