<?php
use App\Models;
use Illuminate\Database\Seeder;

class CabeceraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */


    public function run()
    {
         $valores=['Aluminé A|Aluminé A',
             'Aluminé B|Aluminé B',
             'Andacollo|Andacollo',
             'Barrancas - Buta R.|Barrancas - Buta R.',
             'Centenario A|Centenario A',
             'Centenario B|Centenario B',
             'Chos Malal A|Chos Malal A',
             'Chos Malal B|Chos Malal B',
             'Junín de los Andes A|Junín de los Andes A',
             'Junín de los Andes B|Junín de los Andes B',
             'Las Lajas|Las Lajas',
             'Loncopué|Loncopué',
             'Neuquén J|Neuquén J',
             'Neuquén D|Neuquén D',
             'Neuquén B|Neuquén B',
             'Neuquén G|Neuquén G',
             'Neuquén C|Neuquén C',
             'Neuquén E|Neuquén E',
             'Neuquén I|Neuquén I',
             'Neuquén F|Neuquén F',
             'Neuquén A|Neuquén A',
             'Neuquén H|Neuquén H',
             'Neuquén K|Neuquén K',
             'Picún Leufú |Picún Leufú ',
             'Piedra del Aguila|Piedra del Aguila',
             'Plaza H. - Cutral Co A|Plaza H. - Cutral Co A',
             'Plaza H. - Cutral Co C|Plaza H. - Cutral Co C',
             'Plottier A|Plottier A',
             'Plottier C|Plottier C',
             'Rincón de los Sauces |Rincón de los Sauces ',
             'San Martín de los Andes A|San Martín de los Andes A',
             'San Martín de los Andes C|San Martín de los Andes C',
             'Villa La Angostura |Villa La Angostura ',
             'Zapala A|Zapala A',
             'Zapala C|Zapala C',
             'Zapala D|Zapala D',

         ];

        foreach ($valores as $valor)
        {
            $datos =explode('|', $valor);

            Models\Cabecera::create([
                'descripcion'=>$datos[0],
                'localidad'=>$datos[1],
                'user_id'=>null
            ]);
        }


    }
}
