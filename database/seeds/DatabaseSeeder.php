<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $tables=['establecimientos', 'ambitos', 'cabeceras', 'departamentos', 'localidades',
            'modalidades', 'niveles', 'periodosfuncionamiento', 'niveles', 'sectores', 'participacion',
            'operativos', 'operativos_establecimientos',  'operativos_secciones','operativos_cabeceras'];


        $this->truncateTables($tables);
        //$this->call(UsuarioSeeder::class);
        $this->call(AmbitoSeeder::class);
        $this->call(DepartamentoSeeder::class);
        $this->call(CabeceraSeeder::class);
        $this->call(LocalidadSeeder::class);
        $this->call(ModalidadSeeder::class);
        $this->call(NivelSeeder::class);
        $this->call(PeriodoFuncionamientoSeeder::class);
        $this->call(SectorSeeder::class);
        $this->call(EstablecimientoSeeder::class);
        $this->call(ParticipacionSeeder::class);
        $this->call(OperativoSeeder::class);
        $this->call(OperativoEstablecimientoSeeder::class);
        $this->call(OperativoSeccionSeeder::class);
    }


    public function truncateTables(array $tables)
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;'); // Desactivamos la revisión de claves foráneas

        foreach($tables as $table)
        {
            DB::table($table)->truncate();
        }

        DB::statement('SET FOREIGN_KEY_CHECKS = 1;'); // Desactivamos la revisión de claves foráneas
    }
}
