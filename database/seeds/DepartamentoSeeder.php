<?php

use Illuminate\Database\Seeder;
use App\Models;
class DepartamentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $valores=['ALUMINE',
            'MINAS',
            'CHOS MALAL',
            'PEHUENCHES',
            'CONFLUENCIA',
            'AÑELO',
            'ÑORQUIN',
            'LONCOPUE',
            'HUILICHES',
            'CATAN LIL',
            'COLLON CURA',
            'PICUNCHES',
            'PICUN LEUFU',
            'ZAPALA',
            'LACAR',
            'LOS LAGOS',

        ];

        foreach ($valores as $valor)
        {
            Models\Departamento::create([
                'descripcion'=>$valor,
            ]);
        }



    }
}
