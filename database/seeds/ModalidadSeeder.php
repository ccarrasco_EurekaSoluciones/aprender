<?php

use Illuminate\Database\Seeder;
use App\Models;
class ModalidadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $valores=['Común',
            'Técnica',
            'Agropecuaria',

        ];

        foreach ($valores as $valor)
        {
            Models\Modalidad::create([
                'descripcion'=>$valor,
            ]);
        }
    }
}
