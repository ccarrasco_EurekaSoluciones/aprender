<?php

use Illuminate\Database\Seeder;
use App\Models;

class NivelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $valores=['Primaria',
            'Secundaria',
            'Primario y Secundario',

        ];

        foreach ($valores as $valor)
        {
            /*DB::table($nombretabla)->insert([
                'descripcion'=>$valor,
            ]);*/
            Models\Nivel::create([
                'descripcion'=>$valor,
            ]);
        }
    }
}
