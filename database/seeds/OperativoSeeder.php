<?php

use Illuminate\Database\Seeder;
use App\Models;
use Carbon\Carbon;

class OperativoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $valores=['Aprender 2018|6to|5to|Ciencias Naturales y Ciencias Sociales|Lengua y Matemática|2018-10-23',
        ];
        foreach ($valores as $valor)
        {
            $datos =explode('|', $valor);
            Models\Operativo::create([
                'nombre'=>$datos[0],
                'grado'=>$datos[1],
                'anio'=>$datos[2],
                'materiasgrado'=>$datos[3],
                'materiasanio'=>$datos[4],
                'fecha'=>Carbon::parse($datos[5]),
                'actual'=>true,
                'cerrado'=>false,
            ]);
        }
    }
}
