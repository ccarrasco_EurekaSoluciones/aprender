<?php
use App\Models;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades;

class ParticipacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $valores=['Si',
            'No',
            'Sin Definir',
            'No Corresponde'
        ];

        foreach ($valores as $valor)
        {

            Models\Participacion::create([
                'descripcion'=>$valor,
            ]);
        }
    }
}
