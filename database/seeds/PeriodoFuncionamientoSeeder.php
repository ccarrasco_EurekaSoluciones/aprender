<?php

use Illuminate\Database\Seeder;
use App\Models;

class PeriodoFuncionamientoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $valores=['Febrero - Diciembre',
            'Marzo - Diciembre',
            'Septiembre - Mayo',

        ];

        foreach ($valores as $valor)
        {
            Models\PeriodoFuncionamiento::create([
                'descripcion'=>$valor,
            ]);
        }
    }
}
