<?php

use Illuminate\Database\Seeder;
use App\Models;
class SectorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $valores=['Estatal',
            'Privado',
        ];

        foreach ($valores as $valor)
        {

            Models\Sector::create([
                'descripcion'=>$valor,
            ]);
        }
    }
}
