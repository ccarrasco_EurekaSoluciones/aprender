<?php

use Illuminate\Database\Seeder;

class UsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
          'login'=>'admin',
            'password'=>bcrypt("123456"),
            'esAdmin' => '1',
            'email' => 'ccarrasco@eurekasoluciones.com.ar',
            'apellido' => 'Carrasco Admin',
            'nombre' => 'Claudia'

          ]
        );

      App\User::create([
          'login'=>'usuario',
          'password'=>bcrypt("123456"),
          'esAdmin' => '0',
          'email' => 'claudia.carrasco@eurekasoluciones.com.ar',
          'apellido' => 'Carrasco',
          'nombre' => 'Claudia'

        ]
      );

        App\User::create([
                'login'=>'fansaloni',
                'password'=>bcrypt("fansaloni"),
                'esAdmin' => '1',
                'email' => 'ccarrasco@eurekasoluciones.com.ar',
                'apellido' => 'Ansaloni',
                'nombre' => 'Florencia'

            ]
        );

        App\User::create([
                'login'=>'jsasso',
                'password'=>bcrypt("jsasso"),
                'esAdmin' => '0',
                'email' => 'ccarrasco@eurekasoluciones.com.ar',
                'apellido' => 'Sasso',
                'nombre' => 'Julieta'

            ]
        );
    }
}
