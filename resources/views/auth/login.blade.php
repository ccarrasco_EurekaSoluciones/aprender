@extends('adminlte::layouts.auth')

@section('htmlheader_title')
    Log in
@endsection

@section('content')
    <body class="hold-transition login-page">
        <div id="app" v-cloak>
            <div class="login-logo">
                <img src="img/Logos oficiales.png">
                <br>
                <a href="{{ url('/home') }}"> <img src="img/aprender.png" alt=""></a>
            </div><!-- /.login-logo -->
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-1">
                </div>
                <div class="col-md-5">
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title"><strong>APRENDER</strong></h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="box-group" id="accordion">
                                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                                <div class="panel box box-primary">
                                    <div class="box-header with-border">
                                        <h4 class="box-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" class="collapsed">
                                                ¿Qué es?
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                        <div class="box-body">
                                            Es el dispositivo nacional de evaluación de los aprendizajes de los estudiantes y de generación de información acerca de algunas condiciones en las que se desarrollan.Es el dispositivo nacional de evaluación de los aprendizajes de los estudiantes y de generación de información acerca de algunas condiciones en las que se desarrollan.
                                        </div>
                                    </div>
                                </div>
                                <div class="panel box box-info">
                                    <div class="box-header with-border">
                                        <h4 class="box-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed" aria-expanded="false">
                                                ¿Para qué sirve?
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                        <div class="box-body">
                                            <ul class="list-unstyled">
                                                <li>
                                                    <ul>
                                                        <li>Para brindar insumos que orienten la toma de decisiones para la mejora continua de la educación.</li>
                                                        <li>Para aportar a la comunidad educativa información relevante sobre avances y desafíos en los aprendizajes.</li>
                                                        <li>Para identificar y dimensionar diversos factores que inciden en el proceso educativo</li>
                                                    </ul>
                                                </li>
                                            </ul>

                                        </div>
                                    </div>
                                </div>
                                <div class="panel box box-success">
                                    <div class="box-header with-border">
                                        <h4 class="box-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="collapsed" aria-expanded="false">
                                                ¿A quiénes se evaluará?
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse" aria-expanded="false">
                                        <div class="box-body">
                                            <ul class="list-unstyled">
                                                <li>
                                                    <ul>
                                                        <li>Estudiantes de 5º año de secundaria.</li>

                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel box box-warning">
                                    <div class="box-header with-border">
                                        <h4 class="box-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" class="collapsed" aria-expanded="false">
                                                ¿Qué se evaluará?
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseFour" class="panel-collapse collapse" aria-expanded="false">
                                        <div class="box-body">
                                            <ul class="list-unstyled">
                                                <li><b>5º Año de secundaria.</b>
                                                    <ul>
                                                        <li>Lengua</li>
                                                        <li>Matemática</li>
                                                    </ul>
                                                </li>
                                                <li><b>5º Año de secundaria - Muestra</b>
                                                    <ul>
                                                        <li>Ciencias Naturales</li>
                                                        <li>Ciudadanía</li>
                                                    </ul>
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel box box-danger">
                                    <div class="box-header with-border">
                                        <h4 class="box-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive" class="collapsed" aria-expanded="false">
                                                ¿Cuándo se evaluará?
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseFive" class="panel-collapse collapse" aria-expanded="false">
                                        <div class="box-body">
                                            <ul>
                                                <li><b>Martes 03 de Setiembre</b></li>
                                                <li><b>Miércoles 04 de Setiembre (Muestra)</b></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <div class="col-md-5">
                    <div class="box box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title"><strong>Login</strong></h3>
                        </div>


                        <div class="box-body">
                            <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                                {{ csrf_field() }}

                                <div class="form-group{{ $errors->has('login') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-4 control-label">Usuario</label>

                                    <div class="col-md-6">
                                        <input id="email" class="form-control" name="login" value="{{ old('login') }}" required autofocus>

                                        @if ($errors->has('login'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('login') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" class="col-md-4 control-label">Password</label>

                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control" name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>



                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            Login
                                        </button>


                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('adminlte::layouts.partials.scripts_auth')
    </body>

@endsection