@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{$title}}
@endsection

@section('contentheader_title')
    {{$title}}
@endsection

@section('main-content')
    <form method="post" action="{{ url("cabeceras/{$cabecera->id}")}}">
        {{ method_field('PUT') }}
        {{csrf_field()}}
        <div class="container-fluid spark-screen">
            <div class="row">
                <div class="form-group">
                    <label for="txDescripcion">Descripción</label>
                    <input type="text" id="txDescripcion" name="descripcion" class="form-control"
                           placeholder="Ingrese descripción de la cabecera"  value="{{ old('descripcion', $cabecera->descripcion) }}"
                            required>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label for="txLocalidad">Localidad</label>
                    <input type="text" id="txLocalidad" name="localidad" class="form-control"
                           placeholder="Ingrese localidad"  value="{{ old('localidad', $cabecera->localidad) }}"
                           required>
                </div>
            </div>
            <div class="row">
            <div class="form-group">
                <div class="col-md-2">
                    <label for="user" class="control-label">Coordinador Asig.</label>
                </div>
                <div class="col-md-4">
                    {!! Form::select('user_id', [null=>'Seleccione Usuario'] + $users, $usuario, ['class' => 'form-control']) !!}
                </div>
            </div>
            </div>

            <div class="row pull-right">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <a href="{{ url('cabeceras') }}" class="btn btn-info">Volver</a>
            </div>
        </div>
    </form>

@endsection

