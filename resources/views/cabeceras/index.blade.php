
@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{$title}}
@endsection

@section('contentheader_title')

@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                        <div class="col-md-10">{{$title}}
                        </div>
                        <div class="col-md-2"><a href="{!! url('cabeceras\nuevo')  !!}"><i class='fa fa-plus-square'></i><span> Nueva</span></a></li>

                        </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="box-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Descripción</th>
                                        <th>Localidad</th>
                                        <th>Coordinador Asig.</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($cabeceras as $cabecera)
                                        <tr>
                                            <td>{{ $cabecera->descripcion }}</td>
                                            <td>{{ $cabecera->localidad }}</td>
                                            @if ($cabecera->user!=null)
                                                <td>{{ $cabecera->user->nombre . " " . $cabecera->user->apellido }}</td>
                                            @else
                                                <td></td>
                                            @endif
                                            <td >

                                                <a href="{{ route('cabeceras.edit', $cabecera) }}" class="btn btn-info" title="Modificar"><i class="fa fa-edit"></i></a>
                                                <button type="button" class="btn btn-danger"><i class="fa fa-trash"></i></button>

                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td >
                                                No hay registros configurados.
                                            </td>

                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div><!-- /.box-body -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection