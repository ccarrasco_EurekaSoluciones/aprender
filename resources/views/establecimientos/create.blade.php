
@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{$title}}
@endsection

@section('contentheader_title')
    {{$title}}
@endsection

@section('main-content')
    <form method="post" action="{{ url('establecimientos') }}">
        {{csrf_field()}}
        <div class="box box-default">
            <div class="box-body">
                <div class="box-info">
            <div class="row clearfix">
                <div class="form-group">
                    <div class="col-md-2">
                        <label for="txCUE">CUE</label>
                    </div>
                    <div class="col-md-3">
                        <input type="text" id="txCUE" name="cue" class="form-control"
                               placeholder="Ingrese CUE"  required>
                    </div>

                </div>
                <div class="form-group">
                    <div class="col-md-2">
                        <label for="txNombre">Nombre</label>
                    </div>
                    <div class="col-md-4">
                     <input type="text" id="txNombre" name="nombre" class="form-control"
                           placeholder="Ingrese Nombre"  required>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="form-group">
                    <div class="col-md-2">
                    <label for="localidad" class="control-label">Localidad</label>
                    </div>
                    <div class="col-md-4">
                        {!! Form::select('localidad', [null=>'Seleccione Localidad'] + $localidades, null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-2">
                        <label for="departamento" class="control-label">Departamento</label>
                    </div>
                    <div class="col-md-4">
                        {!! Form::select('departamento', [null=>'(Todos)'] + $departamentos, null, ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="form-group">
                    <div class="col-md-2">
                    <label for="txDomicilio">Domicilio</label>
                    </div>
                    <div class="col-md-4">
                    <input type="text" id="txDomicilio" name="domicilio" class="form-control"
                           placeholder="Ingrese Domicilio"  required>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">
                    <label for="txTelefono">Teléfono</label>
                    </div>
                    <div class="col-md-4">
                    <input type="text" id="txTelefono" name="telefono" class="form-control"
                           placeholder="Ingrese Teléfono" >
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="form-group">
                    <div class="col-md-2">
                        <label for="txMail">Mail</label>
                    </div>
                    <div class="col-md-6">
                        <input type="text" id="txMail" name="email" class="form-control"
                           placeholder="Ingrese Mail" >
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="form-group">
                    <div class="col-md-2">
                    <label for="sector" class="control-label">Sector</label>
                    </div>
                    <div class="col-md-4">
                        {!! Form::select('sector', [null=>'Seleccione Sector'] + $sectores, null, ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-2">
                    <label for="ambito" class="control-label">Ámbito</label>
                    </div>
                    <div class="col-md-4">
                        {!! Form::select('ambito', [null=>'Seleccione Ámbito'] + $ambitos, null, ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <div class="form-group">
                    <div class="col-md-2">
                    <label for="pf" class="control-label">Período Func.</label>
                    </div>
                    <div class="col-md-4">
                        {!! Form::select('pf', [null=>'Seleccione Período Func.'] + $pfs, null, ['class' => 'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">
                    <label for="nivel" class="col-md-4 control-label">Nivel</label>
                    </div>
                    <div class="col-md-4">
                        {!! Form::select('nivel', [null=>'Seleccione Nivel'] + $niveles, null, ['class' => 'form-control']) !!}
                    </div>
                </div>

            </div>
            <div class="row clearfix">
                <div class="form-group">
                    <div class="col-md-2">
                    <label for="modalidad" class="control-label">Modalidad</label>
                    </div>
                    <div class="col-md-4">
                        {!! Form::select('modalidad', [null=>'Seleccione Modalidad'] + $modalidades, null, ['class' => 'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-2">
                    <label for="txDistrito">Distrito</label>
                    </div>
                    <div class="col-md-2">
                    <input type="text" id="txDistrito" name="distrito" class="form-control"
                           placeholder="Ingrese Distrito"  required>
                    </div>
                </div>
            </div>
                <div class="row clearfix">
                    <div class="form-group">
                        <div class="col-md-2">
                            <label for="txCodSupervisor">Cod. Supervisor</label>
                        </div>
                        <div class="col-md-2">
                            <input type="text" id="txCodSupervisor" name="codsupervisor" class="form-control"
                                   placeholder="Ingrese Cod. Supervisor"  required>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-2">
                            <label for="txNombreSupervisor">Nombre Supervisor</label>
                        </div>
                        <div class="col-md-4">
                            <input type="text" id="txNombreSupervisor" name="nombresupervisor" class="form-control"
                                   placeholder="Ingrese Nombre Supervisor"  required>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="form-group">
                        <div class="col-md-2">
                        <label for="cabecera" class="control-label">Cabecera</label>
                        </div>
                        <div class="col-md-4">
                            {!! Form::select('cabecera', [null=>'Seleccione Cabecera'] + $cabeceras, null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
             </div>



            <div class="row clearfix pull-right">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <a href="{{ url('establecimientos') }}" class="btn btn-info">Volver</a>
            </div>
        <div class="row clearfix pull-right">

        </div>
                </div>
            </div>
        </div>
    </form>

@endsection