@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{$title}}
@endsection

@section('contentheader_title')
    {{$title}}
@endsection

@section('main-content')

     {!!Form::model($establecimiento, ['method' => 'POST', 'url' => 'establecimientos/'.$establecimiento->id])!!}
        <input type="hidden" name="_method" value="PUT">
        {{csrf_field()}}

              <div class="box box-default">
          <div class="box-body">
              <div class="box-info">
                  <div class="row clearfix">
                      <div class="form-group">
                          <div class="col-md-2">
                              <label for="txCUE">CUE</label>
                          </div>
                          <div class="col-md-3">
                              {!! Form::text('cue', null, ['class'=>'form-control', 'required']) !!}
                          </div>

                      </div>
                      <div class="form-group">
                          <div class="col-md-2">
                              <label for="txNombre">Nombre</label>
                          </div>
                          <div class="col-md-4">
                              {!! Form::text('nombre', null, ['class'=>'form-control' , 'required']) !!}

                          </div>
                      </div>
                  </div>
                  <div class="row clearfix">
                      <div class="form-group">
                          <div class="col-md-2">
                              <label for="localidad" class="control-label">Localidad</label>
                          </div>
                          <div class="col-md-4">
                              {!! Form::select('localidad_id', [null=>'Seleccione Localidad'] + $localidades,  $establecimiento->localidad_id, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-2">
                                <label for="departamento" class="control-label">Departamento</label>
                            </div>
                            <div class="col-md-4">
                                {!! Form::select('departamento_id', [null=>'(Todos)'] + $departamentos, $establecimiento->departamento_id, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="form-group">
                            <div class="col-md-2">
                                <label for="txDomicilio">Domicilio</label>
                            </div>
                            <div class="col-md-4">
                                {!! Form::text('domicilio', null, ['class'=>'form-control' , 'required']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                                <label for="txTelefono">Teléfono</label>
                            </div>
                            <div class="col-md-4">
                                {!! Form::text('telefono', null, ['class'=>'form-control' ])!!}
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="form-group">
                            <div class="col-md-2">
                                <label for="txMail">Mail</label>
                            </div>
                            <div class="col-md-6">

                                {!! Form::email('email', null, ['class' => 'form-control']) !!}

                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="form-group">
                            <div class="col-md-2">
                                <label for="sector" class="control-label">Sector</label>
                            </div>
                            <div class="col-md-4">
                                {!! Form::select('sector_id', [null=>'Seleccione Sector'] + $sectores,  $establecimiento->sector_id, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-2">
                                <label for="ambito" class="control-label">Ámbito</label>
                            </div>
                            <div class="col-md-4">
                                {!! Form::select('ambito_id', [null=>'Seleccione Ámbito'] + $ambitos,  $establecimiento->ambito_id, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="form-group">
                            <div class="col-md-2">
                                <label for="pf" class="control-label">Período Func.</label>
                            </div>
                            <div class="col-md-4">
                                {!! Form::select('periodofuncionamiento_id', [null=>'Seleccione Período Func.'] + $pfs,  $establecimiento->periodofuncionamiento_id, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                                <label for="nivel" class="col-md-4 control-label">Nivel</label>
                            </div>
                            <div class="col-md-4">
                                {!! Form::select('nivel_id', [null=>'Seleccione Nivel'] + $niveles,  $establecimiento->nivel_id, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                    </div>
                    <div class="row clearfix">
                        <div class="form-group">
                            <div class="col-md-2">
                                <label for="modalidad" class="control-label">Modalidad</label>
                            </div>
                            <div class="col-md-4">
                                {!! Form::select('modalidad_id', [null=>'Seleccione Modalidad'] + $modalidades,  $establecimiento->modalidad_id, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                                <label for="txDistrito">Distrito</label>
                            </div>
                            <div class="col-md-2">
                                {!! Form::text('distrito', null, ['class'=>'form-control' , 'required']) !!}

                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="form-group">
                            <div class="col-md-2">
                                <label for="txCodSupervisor">Cod. Supervisor</label>
                            </div>
                            <div class="col-md-2">
                                {!! Form::text('codsupervisor', null, ['class'=>'form-control' , 'required']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                                <label for="txNombreSupervisor">Nombre Supervisor</label>
                            </div>
                            <div class="col-md-4">
                                {!! Form::text('nombresupervisor', null, ['class'=>'form-control' , 'required']) !!}
                            </div>
                        </div>
                    </div>
                  <div class="row clearfix">
                        <div class="form-group">
                            <div class="col-md-2">
                                <label for="cabecera" class="control-label">Cabecera</label>
                            </div>
                            <div class="col-md-4">
                                {!! Form::select('cabecera_id', [null=>'Seleccione Cabecera'] + $cabeceras,  $establecimiento->cabecera_id, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>



                    <div class="row clearfix pull-right">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <a href="{{ url('establecimientos') }}" class="btn btn-info">Volver</a>
                    </div>
                    <div class="row clearfix pull-right">

                    </div>
                </div>
            </div>
        </div>

    </form>

@endsection
