
@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{$title}}
@endsection

@section('contentheader_title')

@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-10">{{$title}}
                            </div>
                            <div class="col-md-2"><a href="{!! url('establecimientos\nuevo')  !!}"><i class='fa fa-plus-square'></i><span> Nuevo</span></a></li>

                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            {!!Form::model(Request::all(),['route' => ['establecimientos'], 'method' => 'GET', 'role'=>'search'])!!}

                                <div class="col-md-6 pull-left">
                                    <div class="form-group">
                                        <label for="nombre" class="col-md-4 control-label">Nombre</label>
                                        <div class="col-md-8">
                                            {!! Form::text('nombre', null, ['class'=>'form-control', 'placeholder'=>'Nombre']) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="localidad" class="col-md-4 control-label">Localidad</label>
                                        <div class="col-md-8">
                                            {!! Form::select('localidad', [null=>'(Todas)'] + $localidades, null, ['class' => 'form-control']) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                    <label for="sector" class="col-md-4 control-label">Sector</label>
                                    <div class="col-md-8">
                                        {!! Form::select('sector', [null=>'(Todos)'] + $sectores, null, ['class' => 'form-control']) !!}
                                    </div>
                                    </div>
                                    <div class="form-group">
                                    <label for="modalidad" class="col-md-4 control-label">Modalidad</label>
                                    <div class="col-md-8">
                                        {!! Form::select('modalidad', [null=>'(Todas)'] + $modalidades, null, ['class' => 'form-control']) !!}
                                    </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="pf" class="col-md-4 control-label">Periodo Func.</label>
                                        <div class="col-md-8">
                                            {!! Form::select('pf', [null=>'(Todos)'] + $pfs, null, ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 pull-left">
                                    <div class="form-group">
                                        <label for="cue" class="col-md-4 control-label">CUE</label>
                                        <div class="col-md-8">
                                            {!! Form::text('cue', null, ['class'=>'form-control', 'placeholder'=>'Cue...']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">

                                    <label for="nivel" class="col-md-4 control-label">Nivel</label>
                                        <div class="col-md-8">
                                            {!! Form::select('nivel', [null=>'(Todos)'] + $niveles, null, ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                    <label for="ambito" class="col-md-4 control-label">Ambito</label>
                                    <div class="col-md-8">
                                        {!! Form::select('ambito', [null=>'(Todos)'] + $ambitos, null, ['class' => 'form-control']) !!}
                                    </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <label for="departamento" class="control-label">Departamento</label>
                                        </div>
                                        <div class="col-md-8">
                                            {!! Form::select('departamento', [null=>'(Todos)'] + $departamentos, null, ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <label for="cabecera" class="control-label">Cabecera</label>
                                        </div>
                                        <div class="col-md-8">
                                            {!! Form::select('cabecera', [null=>'(Todas)'] + $cabeceras, null, ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                </div>

                            <br>
                            <div class="form-group pull-right">
                                <button type="submit" class="btn btn-default">Buscar</button>
                            </div>
                        </div>
                        {!!Form::close() !!}
                        <div class="box-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>CUE</th>
                                        <th>Nombre</th>
                                        <th>Localidad</th>
                                        <th>Nivel</th>
                                        <th>Sector</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($establecimientos as $establecimiento)
                                        <tr>
                                            <td>{{ $establecimiento->cue }}</td>
                                            <td>{{ $establecimiento->nombre }}</td>
                                            <td>{{ $establecimiento->localidad->descripcion }}</td>
                                            <td>{{ $establecimiento->nivel->descripcion }}</td>
                                            <td>{{ $establecimiento->sector->descripcion }}</td>
                                            <td >

                                                <a href="{{ route('establecimientos.edit', $establecimiento) }}" class="btn btn-info" title="Modificar"><i class="fa fa-edit"></i></a>
                                                <button type="button" class="btn btn-danger"><i class="fa fa-trash"></i></button>

                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td >
                                                No hay registros configurados.
                                            </td>

                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                                {!! $establecimientos->render() !!}
                            </div>
                        </div><!-- /.box-body -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection