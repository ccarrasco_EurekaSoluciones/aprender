@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{$title}}
@endsection

@section('contentheader_title')
    {{$title}}
@endsection

@section('main-content')
    <form method="post" action="{{ url("localidades/{$localidad->id}")}}">
        {{ method_field('PUT') }}
        {{csrf_field()}}
        <div class="container-fluid spark-screen">
            <div class="row">
                <div class="form-group">
                    <label for="txDescripcion">Descripción</label>
                    <input type="text" id="txDescripcion" name="descripcion" class="form-control"
                           placeholder="Ingrese descripción de la localidad"  value="{{ old('descripcion', $localidad->descripcion) }}"
                            required>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label for="txCaracteristica">Caract. Telef.</label>
                    <input type="text" id="txCaracteristica" name="caracteristicatelefonica" class="form-control"
                           placeholder="Ingrese característica telefónica"  value="{{ old('caracteristicatelefonica', $localidad->caracteristicatelefonica) }}"
                           required>
                </div>
            </div>
            <div class="row pull-right">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <a href="{{ url('localidades') }}" class="btn btn-info">Volver</a>
            </div>
        </div>
    </form>

@endsection

