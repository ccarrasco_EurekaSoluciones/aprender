
@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{$title}}
@endsection

@section('contentheader_title')
    {{$title}}
@endsection

@section('main-content')
    <form method="post" action="{{ url('operativos') }}">
        {{csrf_field()}}
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="form-group">
                <label for="txNombre">Nombre</label>
                <input type="text" id="txNombre" name="nombre" class="form-control"
                       placeholder="Ingrese nombre del operativo"  required>
            </div>
            <div class="form-group">
                <label for="txGrado">Grado Primaria a Evaluar</label>
                <input type="text" id="txGrado" name="grado" class="form-control"
                       placeholder="Ingrese el/los grados a evaluar de primaria"  required>
            </div>
            <div class="form-group">
                <label for="txAnio">Año Secundaria a Evaluar</label>
                <input type="text" id="txAnio" name="anio" class="form-control"
                       placeholder="Ingrese el/los años a evaluar de secundaria"  required>
            </div>
            <div class="form-group">
                <label for="txMateriasPrimaria">Materias a evaluar Primaria</label>
                <input type="text" id="txMateriasPrimaria" name="materiasgrado" class="form-control"
                       placeholder="Ingrese las materia a evaluar de primaria"  required>
            </div>
            <div class="form-group">
                <label for="txMateriasSecundaria">Materias a evaluar Secundaria</label>
                <input type="text" id="txMateriasSecundaria" name="materiasanio" class="form-control"
                       placeholder="Ingrese las materia a evaluar de secundaria"  required>
            </div>
            <div class="form-group">
                <label for="txFecha">Fecha del Operativo</label>
                <input type="date" id="txFecha" name="fecha" class="form-control"
                       placeholder="Ingrese la fecha en la que se evaluará"  required>
            </div>
        </div>
        <div class="row pull-right">
            <button type="submit" class="btn btn-primary">Guardar</button>
            <a href="{{ url('operativos') }}" class="btn btn-info">Volver</a>
        </div>
    </div>
    </form>

@endsection