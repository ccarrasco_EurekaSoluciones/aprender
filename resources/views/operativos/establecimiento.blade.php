@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{$title}}
@endsection

@section('contentheader_title')

@endsection

@section('main-content')

    {!!Form::model($establecimiento, ['method' => 'POST', 'url' => 'operativos/guardaractualizacionestablecimiento'])!!}
    <input type="hidden" name="_method" value="POST">
    {!! Form::hidden('operativo_establecimiento_id', $establecimientoOperativo->id) !!}
    {!! Form::hidden('operativo_id', $operativo->id) !!}
    {!! Form::hidden('cabecera_id', $establecimiento->cabecera_id) !!}
    {{csrf_field()}}

    <div class="box box-default">
        <div class="box-info">
            <div class="box-body">
                <div class="form-horizontal">

                        <div class="form-group">
                            <div class="col-md-2">
                                <label for="txCUE">CUE</label>
                            </div>
                            <div class="col-md-2">
                                {!! Form::text('cue', null, ['class'=>'form-control', 'required']) !!}
                            </div>


                            <div class="col-md-2">
                                <label for="txNombre">Nombre</label>
                            </div>
                            <div class="col-md-6">
                                {!! Form::text('nombre', null, ['class'=>'form-control' , 'required']) !!}

                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-2">
                                <label for="localidad" class="control-label">Localidad</label>
                            </div>
                            <div class="col-md-4">
                                {!! Form::text('localidad', $establecimiento->localidad->descripcion, ['class' => 'form-control']) !!}
                            </div>

                            <div class="col-md-2">
                                <label for="departamento" class="control-label">Departamento</label>
                            </div>
                            <div class="col-md-4">
                                {!! Form::text('departamento', $establecimiento->departamento->descripcion, ['class' => 'form-control']) !!}
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-2">
                                <label for="Domicilio">Domicilio</label>
                            </div>
                            <div class="col-md-4">
                                {!! Form::text('domicilio', null, ['class'=>'form-control' , 'required']) !!}
                            </div>

                            <div class="col-md-2">
                                <label for="Telefono">Teléfono</label>
                            </div>
                            <div class="col-md-4">
                                {!! Form::text('telefono', null, ['class'=>'form-control' ])!!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                                <label for="txMail">Mail</label>
                            </div>
                            <div class="col-md-6">

                                {!! Form::email('email', null, ['class' => 'form-control']) !!}

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-2">
                                <label for="sector" class="control-label">Sector</label>
                            </div>
                            <div class="col-md-4">
                                {!! Form::text('sector', $establecimiento->sector->descripcion, ['class' => 'form-control']) !!}
                            </div>

                            <div class="col-md-2">
                                <label for="nivel" class="control-label">Nivel</label>
                            </div>
                            <div class="col-md-4">
                                {!! Form::text('nivel',  $establecimiento->nivel->descripcion, ['class' => 'form-control']) !!}
                            </div>


                        </div>

                    <div class="form-group">
                        <div class="col-md-2">
                            <label for="modalidad" class="control-label">Modalidad</label>
                        </div>
                        <div class="col-md-4">
                            {!! Form::text('modalidad',  $establecimiento->modalidad->descripcion, ['class' => 'form-control']) !!}
                        </div>

                        <div class="col-md-2">
                            <label for="ambito" class="control-label">Ámbito</label>
                        </div>
                        <div class="col-md-4">
                            {!! Form::text('ambito', $establecimiento->ambito->descripcion, ['class' => 'form-control']) !!}
                        </div>


                    </div>

                        <div class="form-group">
                            <div class="col-md-2">
                                <label for="pf" class="control-label">Período Func.</label>
                            </div>
                            <div class="col-md-4">
                                {!! Form::text('pf', $establecimiento->periodofuncionamiento->descripcion, ['class' => 'form-control']) !!}
                            </div>

                            <div class="col-md-2">
                                <label for="cabecera" class="control-label">Cabecera</label>
                            </div>
                            <div class="col-md-4">
                                {!! Form::select('cabecera',  $cabeceras,  $establecimiento->cabecera_id, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                    <div class="form-group">
                        <div class="col-md-2">
                            <label for="distrito">Distrito</label>
                        </div>
                        <div class="col-md-2">
                            {!! Form::text('distrito', $establecimiento->distrito, ['class'=>'form-control' , 'required']) !!}

                        </div>
                    </div>

                    <div class="form-group">
                            <div class="col-md-2">
                                <label for="codsupervisor">Cod. Supervisor</label>
                            </div>
                            <div class="col-md-2">
                                {!! Form::text('codsupervisor', $establecimientoOperativo->codsupervisor, ['class'=>'form-control' , 'required']) !!}
                            </div>
                            <div class="col-md-2">
                                <label for="nombresupervisor">Nombre Supervisor</label>
                            </div>
                            <div class="col-md-4">
                                {!! Form::text('nombresupervisor', $establecimientoOperativo->nombresupervisor, ['class'=>'form-control' , 'required']) !!}
                            </div>

                        </div>

                    <div class="form-group">
                        <div class="col-md-2">
                            <label for="director">Director</label>
                        </div>
                        <div class="col-md-4">
                            {!! Form::text('director', $establecimientoOperativo->director, ['class'=>'form-control']) !!}
                        </div>
                        <div class="col-md-2">
                            <label for="directortelefono">Director - Teléfono</label>
                        </div>
                        <div class="col-md-4">
                            {!! Form::text('directortelefono', $establecimientoOperativo->directortelefono, ['class'=>'form-control']) !!}
                        </div>

                    </div>




                    <ul class="timeline">

                        <!-- timeline time label -->
                        <li class="time-label">
                        <span class="bg-red">
                            Participación
                        </span>
                        </li>
                        <!-- /.timeline-label -->

                        <!-- timeline item -->

                        <li>
                            <!-- timeline icon -->
                            <i class="fa fa-thumb-tack bg-blue"></i>
                            <div class="timeline-item">

                                <h3 class="timeline-header">
                                    <div class="form-group">
                                        <div class="col-md-2">
                                            <label for="participa">Participa?</label>
                                        </div>
                                        <div class="col-md-2">
                                            {!! Form::select('participaestablecimiento',  $participaciones,  $establecimientoOperativo->participacion_id, ['class' => 'form-control',  'id'=>'participaestablecimiento' , 'onchange'=>'modificarParticipacion();']) !!}
                                        </div>
                                    </div>
                                </h3>

                            </div>
                        </li><!-- END timeline item -->

                        <li>
                            <!-- timeline icon -->
                            <i class="fa fa-users bg-aqua"></i>
                            <div class="timeline-item">

                                <h3 class="timeline-header">
                                    <div class="form-group">
                                        <div class="col-md-2">
                                            <label for="veedor">Veedor</label>
                                        </div>
                                        <div class="col-md-6">
                                            @if ($establecimientoOperativo->participacion_id ==1)
                                                {!! Form::text('veedor', $establecimientoOperativo->veedor, ['class'=>'form-control', 'id'=>'veedor']) !!}
                                            @else
                                                {!! Form::text('veedor', $establecimientoOperativo->veedor, ['class'=>'form-control', 'id'=>'veedor', 'disabled'=>'true']) !!}
                                            @endif
                                        </div>

                                    </div>
                                </h3>
                                <div class="timeline-body">
                                    <div class="form-group">

                                        <div class="col-md-2">
                                            <span>Teléfono: </span>
                                        </div>
                                        <div class="col-md-4">
                                            @if ($establecimientoOperativo->participacion_id ==1)
                                                {!! Form::text('veedor_telefono', $establecimientoOperativo->veedor_telefono, ['class'=>'form-control', 'id'=>'veedor_telefono']) !!}
                                            @else
                                                {!! Form::text('veedor_telefono', $establecimientoOperativo->veedor_telefono, ['class'=>'form-control', 'id'=>'veedor_telefono', 'disabled'=>'true']) !!}
                                            @endif
                                        </div>


                                        <div class="col-md-2">
                                            <span>E-Mail: </span>
                                        </div>
                                        <div class="col-md-4">
                                            @if ($establecimientoOperativo->participacion_id ==1)
                                                {!! Form::text('veedor_mail', $establecimientoOperativo->veedor_mail, ['class'=>'form-control', 'id'=>'veedor_mail']) !!}
                                            @else
                                                {!! Form::text('veedor_mail', $establecimientoOperativo->veedor_mail, ['class'=>'form-control', 'id'=>'veedor_mail', 'disabled'=>'true']) !!}
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">

                                        <div class="col-md-2">
                                            <span>DNI: </span>
                                        </div>
                                        <div class="col-md-4">
                                            @if ($establecimientoOperativo->participacion_id ==1)
                                                {!! Form::number('veedor_dni',  $establecimientoOperativo->veedor_dni, ['class'=>'form-control', 'id'=>'veedor_dni']) !!}
                                            @else
                                                {!! Form::number('veedor_dni',  $establecimientoOperativo->veedor_dni, ['class'=>'form-control', 'id'=>'veedor_dni', 'disabled'=>'true']) !!}
                                            @endif
                                        </div>
                                        <div class="col-md-2">
                                            <span>Nro. Empleado: </span>
                                        </div>
                                        <div class="col-md-4">
                                            @if ($establecimientoOperativo->participacion_id ==1)
                                                {!! Form::text('veedor_nroempleado', $establecimientoOperativo->veedor_nroempleado, ['class'=>'form-control', 'id'=>'veedor_nroempleado']) !!}
                                            @else
                                                {!! Form::text('veedor_nroempleado', $establecimientoOperativo->veedor_nroempleado, ['class'=>'form-control', 'id'=>'veedor_nroempleado', 'disabled'=>'true']) !!}
                                            @endif
                                        </div>
                                    </div>


                                </div>


                            </div>
                        </li>


                        <li>
                            <!-- timeline icon -->
                            <i class="fa fa-dropbox bg-red"></i>
                            <div class="timeline-item">

                                <h3 class="timeline-header">
                                    <div class="form-group">
                                        <div class="col-md-2">
                                            <label>Material</label>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                    <div class="col-md-2">
                                        <label for="retiromaterial">Retiro Material?</label>
                                    </div>
                                    <div class="col-md-2">
                                        {!! Form::select('retiromaterial',  array('0' => 'NO', '1' => 'SI'),  $establecimientoOperativo->retiromaterial, ['class' => 'form-control',  'id'=>'retiromaterial' , 'onchange'=>'modificarRetiroMaterial();']) !!}
                                    </div>
                                    <div class="col-md-3">
                                        <label for="retiromaterialcajas">Cantidad de cajas retiradas</label>
                                    </div>
                                    <div class="col-md-2">
                                        @if($establecimientoOperativo->retiromaterial =="0")
                                            {!! Form::number('retiromaterialcajas',  $establecimientoOperativo->retiromaterialcajas, ['class' => 'form-control',  'id'=>'retiromaterialcajas' , 'disabled'=>'true']) !!}
                                        @else
                                            {!! Form::number('retiromaterialcajas',  $establecimientoOperativo->retiromaterialcajas, ['class' => 'form-control',  'id'=>'retiromaterialcajas'  ]) !!}
                                        @endif
                                    </div>
                                    </div>
                                </h3>

                            </div>
                        </li><!-- END timeline item -->

                        <li>
                            <!-- timeline icon -->
                            <i class="fa  fa-money bg-green"></i>
                            <div class="timeline-item">

                                <h3 class="timeline-header">
                                    <div class="form-group">
                                        <div class="col-md-2">
                                            <label>Pago</label>
                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <div class="col-md-2">
                                            <span>Pagar? </span>
                                        </div>
                                        <div class="col-md-2">
                                            {!! Form::select('pagar', array('0' => 'NO', '1' => 'SI'), $establecimientoOperativo->pagar, [ 'class'=>'form-control','id'=>'pagar']) !!}

                                        </div>
                                    </div>
                                </h3>

                            </div>
                        </li>
                    </ul>

                    <div class="form-group pull-right">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <a href="{{ route('operativo.mostrarDatosCabecera',['operativo' => $operativo, 'cabecera' => $cabecera ] ) }}" class="btn btn-info" title="Volver">Volver</a>
                    </div>

                </div>
            </div>
        </div>


    </div>
    <h4 class="box-title">Secciones </h4>

    <div class="box box-default">
    <div class="box-body">
        <div class="box-info">
            <div class="row clearfix">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Nivel</th>
                            <th>Grado/Año</th>
                            <th>División</th>
                            <th>Turno</th>
                            <th>Matr.</th>
                            <th>Participa</th>
                            <th>Matr. a Evaluar</th>
                            <th>Aplicador</th>
                            <th>TE. Aplicador</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($secciones as $seccion)
                            <tr>
                                <td>{{ $seccion->pivot->nivel}}</td>
                                <td>{{ $seccion->pivot->gradooanio }}</td>
                                <td>{{ $seccion->pivot->seccion }}</td>
                                <td>{{ $seccion->pivot->turno }}</td>
                                <td>{{ $seccion->pivot->matricula }}</td>
                                <td>@if($seccion->pivot->participacion_id==1)
                                    <span class="label label-success">SI</span>
                                @else
                                    @if($seccion->pivot->participacion_id==2)
                                        <span class="label label-danger">NO</span>
                                    @else
                                        @if($seccion->pivot->participacion_id==3)
                                            <span class="label label-warning">Sin Definir</span>
                                        @else
                                            <span class="label label-info">No Corresponde</span>
                                            @endif
                                            @endif
                                            @endif</td>

                                <td>{{ $seccion->pivot->matriculaaevaluar }}</td>
                                <td>{{ $seccion->pivot->aplicador }}</td>
                                <td>{{ $seccion->pivot->aplicador_telefono }}</td>
                                <td>
                                    <button
                                        type="button"
                                        class="btn btn-primary btn-sm"
                                        data-toggle="modal"
                                        data-id="{{ $seccion->pivot->id }}"
                                        data-establecimiento="{{ $establecimiento->nombre }}"
                                        data-seccion="{{ $seccion->pivot->seccion }}"
                                        data-nivel="{{ $seccion->pivot->nivel }}"
                                        data-grado="{{ $seccion->pivot->gradooanio }}"
                                        data-turno="{{ $seccion->pivot->turno }}"
                                        data-participacion_id="{{ $seccion->pivot->participacion_id }}"
                                        data-matricula="{{ $seccion->pivot->matricula }}"
                                        data-matriculaaevaluar="{{ $seccion->pivot->matriculaaevaluar }}"
                                        data-matriculaevaluada="{{ $seccion->pivot->matriculaevaluada }}"
                                        data-aplicador="{{ $seccion->pivot->aplicador }}"
                                        data-aplicador_telefono="{{ $seccion->pivot->aplicador_telefono }}"
                                        data-aplicador_dni="{{ $seccion->pivot->aplicador_dni }}"
                                        data-aplicador_mail="{{ $seccion->pivot->aplicador_mail }}"
                                        data-aplicador_nroempleado="{{ $seccion->pivot->aplicador_nroempleado }}"
                                        data-aplico="{{ $seccion->pivot->aplico }}"
                                        data-pagar="{{ $seccion->pivot->pagar }}"
                                        data-target="#seccionModal">
                                        Editar
                                    </button>

                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td >
                                    No hay secciones con alumnos ed 6to Grado
                                </td>

                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
    </div>


    </form>
    <div class="modal fade" id="seccionModal"
         tabindex="-1" role="dialog"
         aria-labelledby="seccionModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close"
                            data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"
                        id="seccionModalLabel">El nombre del estalecimiento</h4>
                </div>
                {!!Form::open (['method' => 'POST', 'url' => 'operativos/actualizarseccion', 'id'=> 'formseccion'])!!}
                <input type="hidden" name="_method" value="POST">
                <div class="modal-body">
                    <p>
                        Por favor confirme o modifique los datos:

                    </p>

                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-md-1">
                                <span>Nivel: </span>
                            </div>
                            <div class="col-md-2">
                                <b><span id="sec-nivel">el nivel</span></b>
                            </div>
                            <div class="col-md-2">
                                <span>Grado/Año: </span>
                            </div>
                            <div class="col-md-1">
                                <b><span id="sec-grado">el grado/Anio</span></b>
                            </div>
                            <div class="col-md-2">
                                <span>Sección: </span>
                            </div>
                            <div class="col-md-1">
                                <b><span id="sec-seccion">la sección</span></b>
                            </div>
                            <div class="col-md-2">
                                <span>Turno: </span>
                            </div>
                            <div class="col-md-1">
                                <b><span id="sec-turno">el turno</span></b>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-3">
                                <span>Matrícula: </span>
                            </div>
                            <div class="col-md-1">
                                <b><span id="sec-matricula">la matrícula</span></b>
                            </div>
                            <div class="col-md-8">

                            </div>
                        </div>

                        <div class="form-group">

                            <div class="col-md-3">
                                <span>Participa: </span>
                            </div>
                            <div class="col-md-3">
                                {!! Form::select('participaseccion', $participaciones, null, ['class'=>'form-control', 'id'=>'sec-participacion_id','onchange'=>'modificarParticipacionSeccion();']) !!}
                            </div>
                            <div class="col-md-3">
                                <span>Matrícula a Evaluar: </span>
                            </div>
                            <div class="col-md-2">

                                {!! Form::text('matriculaaevaluar', null, ['class'=>'form-control', 'id'=>'sec-matriculaaevaluar']) !!}
                            </div>
                        </div>


                        <div class="form-group">

                            <div class="col-md-3">
                                <span>Aplicador: </span>
                            </div>
                            <div class="col-md-9">

                                {!! Form::text('aplicador', null, ['class'=>'form-control', 'id'=>'sec-aplicador']) !!}
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-md-2">
                                <span>Teléfono: </span>
                            </div>
                            <div class="col-md-4">

                                {!! Form::text('aplicador_telefono', null, ['class'=>'form-control', 'id'=>'sec-aplicador_telefono']) !!}
                            </div>


                            <div class="col-md-2">
                                <span>E-Mail: </span>
                            </div>
                            <div class="col-md-4">

                                {!! Form::text('aplicador_mail', null, ['class'=>'form-control', 'id'=>'sec-aplicador_mail']) !!}
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-md-2">
                                <span>DNI: </span>
                            </div>
                            <div class="col-md-4">

                                {!! Form::number('aplicador_dni', null, ['class'=>'form-control', 'id'=>'sec-aplicador_dni']) !!}
                            </div>
                            <div class="col-md-2">
                                <span>Nro. Empleado: </span>
                            </div>
                            <div class="col-md-4">

                                {!! Form::number('aplicador_nroempleado', null, ['class'=>'form-control', 'id'=>'sec-aplicador_nroempleado']) !!}
                            </div>
                        </div>

                        <ul class="timeline">
                            <!-- timeline time label -->
                            <li class="time-label">
                  <span class="bg-red">
                    Cierre Operativo
                  </span>
                            </li>
                            <!-- /.timeline-label -->
                            <!-- timeline item -->
                            <li>


                                <div class="timeline-item">

                                    <h3 class="timeline-header"></h3>

                                    <div class="timeline-body">
                                        <div class="form-group">
                                            <div class="col-md-1">
                                                <span>Aplicó? </span>
                                            </div>
                                            <div class="col-md-3">

                                                {!! Form::select('aplico', array('0' => 'NO', '1' => 'SI'), 0, [ 'class'=>'form-control','id'=>'sec-aplico']) !!}

                                            </div>
                                            <div class="col-md-2">
                                                <span>Matrícula Evaluada: </span>
                                            </div>
                                            <div class="col-md-2">
                                                {!! Form::text('matriculaevaluada', null, ['class'=>'form-control', 'id'=>'sec-matriculaevaluada']) !!}
                                            </div>
                                            <div class="col-md-1">
                                                <span>Pagar? </span>
                                            </div>
                                            <div class="col-md-3">

                                                {!! Form::select('pagar', array('0' => 'NO', '1' => 'SI'), 0, [ 'class'=>'form-control','id'=>'sec-pagar']) !!}

                                            </div>
                                        </div>

                                    </div>
                                    <div class="timeline-footer">
                                    </div>
                                </div>
                            </li>
                        </ul>




                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button"
                            class="btn btn-default"
                            data-dismiss="modal">Cancelar</button>
                    <span class="pull-right">
                        {!! Form::hidden('id', '', ['id' => 'sec-id']) !!}
                        <button type="submit" class="btn btn-primary">
                            Guardar
                        </button>


                    </span>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection

<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('js/app.js')}}"></script>
<script type="text/javascript">
    $(function() {
        $('#seccionModal').on("show.bs.modal", function (e) {
            $("#seccionModalLabel").html($(e.relatedTarget).data('establecimiento'));
            $("#sec-nivel").html($(e.relatedTarget).data('nivel'));
            $("#sec-seccion").html($(e.relatedTarget).data('seccion'));
            $("#sec-grado").html($(e.relatedTarget).data('grado'));
            $("#sec-turno").html($(e.relatedTarget).data('turno'));
            $("#sec-matricula").html($(e.relatedTarget).data('matricula'));
            document.getElementById("sec-matriculaaevaluar").value =$(e.relatedTarget).data('matriculaaevaluar');
            document.getElementById("sec-matriculaevaluada").value =$(e.relatedTarget).data('matriculaevaluada');
            $('#sec-participacion_id').val($(e.relatedTarget).data('participacion_id')).change();
            document.getElementById("sec-aplicador").value =$(e.relatedTarget).data('aplicador');
            document.getElementById("sec-aplicador_dni").value =$(e.relatedTarget).data('aplicador_dni');
            document.getElementById("sec-aplicador_mail").value =$(e.relatedTarget).data('aplicador_mail');
            document.getElementById("sec-aplicador_telefono").value =$(e.relatedTarget).data('aplicador_telefono');
            document.getElementById("sec-aplicador_nroempleado").value =$(e.relatedTarget).data('aplicador_nroempleado');

            document.getElementById("sec-aplico").value =$(e.relatedTarget).data('aplico');
            document.getElementById("sec-pagar").value =$(e.relatedTarget).data('pagar');

          /*  if ($(e.relatedTarget).data('aplico')=="1")
            {
                $('#sec-aplico').prop('checked', true);
            }*/

           /* if ($(e.relatedTarget).data('pagar')=="1")
            {
                $('#sec-pagar').prop('checked', true);
            }*/
            $("#sec-id").val($(e.relatedTarget).data('id'));

        });



    });


    function modificarParticipacion()
    {
       /*No o No corresponde: los campos de veedor aparezcan grisados y sin posibilidad de cargar ningún dato.
       En la parte de secciones que se cambié el “Sin definir” por el No o No corresponde,
       según lo que indique en “Participa? Y que se grisen los campos de Aplicador y TE Aplicador
       y no se pueda cargan ningún dato.
       Cuando  indique en “Participa?” SI, el la parte de secciones siga el sin definir hasta que se cambié desde
       el “editar”
        */

         var valorseleccionado=document.getElementById('participaestablecimiento').value;
         if (valorseleccionado=='2' || valorseleccionado=='4')
         {
             document.getElementById('veedor').disabled = true;
             document.getElementById('veedor_telefono').disabled = true;
             document.getElementById('veedor_mail').disabled = true;
             document.getElementById('veedor_dni').disabled = true;
             document.getElementById('veedor_nroempleado').disabled = true;
             document.getElementById('pagar').disabled = true;
             document.getElementById('pagar').value='0';
         }

        if (valorseleccionado=='1')
        {
            document.getElementById('veedor').disabled = false  ;
            document.getElementById('veedor_telefono').disabled = false;
            document.getElementById('veedor_mail').disabled = false;
            document.getElementById('veedor_dni').disabled = false;
            document.getElementById('veedor_nroempleado').disabled = false;
            document.getElementById('pagar').disabled = false;
        }
        // alert (valorseleccionado.value);
    }

    function modificarParticipacionSeccion()
    {
        /*No o No corresponde: los campos de veedor aparezcan grisados y sin posibilidad de cargar ningún dato.
        En la parte de secciones que se cambié el “Sin definir” por el No o No corresponde,
        según lo que indique en “Participa? Y que se grisen los campos de Aplicador y TE Aplicador
        y no se pueda cargan ningún dato.
        Cuando  indique en “Participa?” SI, el la parte de secciones siga el sin definir hasta que se cambié desde
        el “editar”
         */

        var valorseleccionado=document.getElementById('sec-participacion_id').value;
        if (valorseleccionado=='2' || valorseleccionado=='4')
        {
            document.getElementById('sec-aplicador').disabled = true;
            document.getElementById('sec-aplicador_mail').disabled = true;
            document.getElementById('sec-aplicador_dni').disabled = true;
            document.getElementById('sec-aplicador_telefono').disabled = true;
            document.getElementById('sec-aplicador_nroempleado').disabled = true;
            document.getElementById('sec-pagar').disabled = true;
            document.getElementById('sec-aplico').disabled = true;
            document.getElementById('sec-matriculaaevaluar').disabled = true;
            document.getElementById('sec-matriculaaevaluar').value = "0";
            document.getElementById('sec-matriculaevaluada').disabled = true;
            document.getElementById('sec-matriculaevaluada').value="0";
        }

        if (valorseleccionado=='1')
        {
            document.getElementById('sec-aplicador').disabled = false;
            document.getElementById('sec-aplicador_mail').disabled = false;
            document.getElementById('sec-aplicador_dni').disabled = false;
            document.getElementById('sec-aplicador_telefono').disabled = false;
            document.getElementById('sec-aplicador_nroempleado').disabled = false;
            document.getElementById('sec-pagar').disabled = false;
            document.getElementById('sec-aplico').disabled = false;
            document.getElementById('sec-matriculaevaluada').disabled = false;
            document.getElementById('sec-matriculaaevaluar').disabled = false;
        }
        // alert (valorseleccionado.value);
    }

    function modificarRetiroMaterial()
    {

        var valorseleccionado=document.getElementById('retiromaterial').value;
        if (valorseleccionado=='0')
        {
            document.getElementById('retiromaterialcajas').disabled = true;
            document.getElementById('retiromaterialcajas').value="0";
        }
        else
        {
            document.getElementById('retiromaterialcajas').disabled = false;
        }

    }

</script>

