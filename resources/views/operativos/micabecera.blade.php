@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{$title}}
@endsection

@section('contentheader_title')

    <h3 class="box-title">{{$operativo->nombre}}</h3>
    @if ($cabecera->user_id <> null)
        <h4 class="box-title">Cabecera: <b> {{$cabecera->descripcion}} </b>  - Coordinador: <b>{{$coordinador->nombre}} {{$coordinador->apellido}}</b> -  Localidad: <b>{{$cabecera->localidad}}</b> </h4>
    @else
        <h4 class="box-title">Cabecera: <b> {{$cabecera->descripcion}} </b>   - Coordinador: <b>SIN COORDINADOR ASIGNADO</b>  -  Localidad: <b>{{$cabecera->localidad}} </b></h4>
    @endif
    <div class="row">
        <div class="col-lg-3 col-xs-3">
            <a href="{{ route('operativo.ExportarEstablecimientos',['operativo' => $operativo, 'cabecera' => $cabecera]) }}" >

                <img src={{ asset('/img/Excel-icon.png') }} alt=""> <br>Universo de Establecimientos
            </a>
        </div>
        <div class="col-lg-3 col-xs-3">
            <a href="{{ route('operativo.ExportarVeedores',['operativo' => $operativo, 'cabecera' => $cabecera]) }}" >

                <img src={{ asset('/img/Excel-icon.png') }} alt=""> <br>Listado Veedores
            </a>
        </div>
        <div class="col-lg-3 col-xs-3">
            <a href="{{ route('operativo.ExportarAplicadores',['operativo' => $operativo, 'cabecera' => $cabecera]) }}" >

                <img src={{ asset('/img/Excel-icon.png') }} alt=""> <br>Listado Aplicadores
            </a>
        </div>
        <div class="col-lg-3 col-xs-3">
    <a href="{{ route('operativo.ExportarParticipacion',['operativo' => $operativo, 'cabecera' => $cabecera]) }}" >

        <img src={{ asset('/img/Excel-icon.png') }} alt=""> <br>Participación
    </a>
        </div>
    </div>
@endsection

@section('main-content')
    <form method="post" action="{{ url("operativos.{operativo->id}")}}">
        {{ method_field('PUT') }}
        {{csrf_field()}}
        <div class="container-fluid spark-screen">

            {{--Las estadisticas de la cabecera para ese operativo--}}
            <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3>{{ $establecimientos_cantidad }}</h3>
                            <p>Establecimientos</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-university"></i>
                        </div>
                        <a href="#" class="small-box-footer">Nivel Primario: {{ $establecimientosnivelprimario }}</a>
                        <a href="#" class="small-box-footer">Nivel Secundario: {{ $establecimientosnivelsecundario }}</a>
                    </div>
                </div>

                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3> {{ $secciones_cantidad }}</h3>

                            <p>Secciones</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-thumb-tack"></i>
                        </div>
                        <a href="#" class="small-box-footer">Nivel Primario:  {{ $seccionesnivelprimario }}</a>
                        <a href="#" class="small-box-footer">Nivel Secundario:  {{ $seccionesnivelsecundario }}</a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3>{{  $participacionEstablecimientos["veedorSI"] }}</h3>

                            <p>Veedores</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-group"></i>
                        </div>
                        @if($participacionEstablecimientos["veedorNO"]>0)
                            <a href="#" class="small-box-footer">+ Hay {{  $participacionEstablecimientos["veedorNO"] }} veedores sin asignar</a>
                        @endif


                    </div>
                </div>
                <!-- ./col -->

                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3>{{  $participacionSecciones["aplicadorSI"] }}</h3>

                            <p>Aplicadores</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-group"></i>
                        </div>
                        @if($participacionSecciones["aplicadorNO"]>0)
                            <a href="#" class="small-box-footer">+ Hay {{  $participacionSecciones["aplicadorNO"] }} aplicadores sin asignar</a>
                        @endif
                    </div>
                </div>
            </div>

            {{--Las estadisticas de la participacion de esa cabecera--}}
            <div class="row">

                <div class="col-md-4">
                    <div class="box box-info">
                        <div class="box-header ui-sortable-handle" style="cursor: move;">
                            <i class="fa fa-university"></i>

                            <h3 class="box-title"><strong>Participación Establecimientos</strong></h3>
                            <!-- tools box -->

                            <!-- /. tools -->
                        </div>

                        <div class="box box-body">
                            <div class="progress-group">
                                <span class="progress-text">Si: </span>
                                <span class="progress-number"><b>{{  $participacionEstablecimientos["SI"] }}</b>/{{ $establecimientos_cantidad }}</span>

                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-green" style="width: {{ $participacionEstablecimientos["SI"]/ $establecimientos_cantidad*100 }}%"></div>
                                </div>
                            </div>
                            <!-- /.progress-group -->
                            <div class="progress-group">
                                <span class="progress-text">No: </span>
                                <span class="progress-number"><b>{{  $participacionEstablecimientos["NO"] }}</b>/{{ $establecimientos_cantidad }}</span>

                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-red" style="width: {{ $participacionEstablecimientos["NO"]/ $establecimientos_cantidad*100 }}%"></div>
                                </div>
                            </div>
                            <!-- /.progress-group -->
                            <div class="progress-group">
                                <span class="progress-text">Sin Definir</span>
                                <span class="progress-number"><b>{{  $participacionEstablecimientos["SD"] }}</b>/{{ $establecimientos_cantidad }}</span>

                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-yellow" style="width: {{ $participacionEstablecimientos["SD"]/ $establecimientos_cantidad*100 }}%"></div>
                                </div>
                            </div>
                            <!-- /.progress-group -->
                            <div class="progress-group">
                                <span class="progress-text">No Corresponde</span>
                                <span class="progress-number"><b>{{ $participacionEstablecimientos["NC"] }}</b>/{{ $establecimientos_cantidad }}</span>

                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-aqua" style="width: {{  $participacionEstablecimientos["NC"]/ $establecimientos_cantidad*100 }}%"></div>
                                </div>
                            </div>
                        </div>
                    <!-- /.progress-group -->
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="box box-success">
                        <div class="box-header ui-sortable-handle" style="cursor: move;">
                            <i class="fa fa-thumb-tack"></i>

                            <h3 class="box-title"><strong>Participación Secciones</strong></h3>
                            <!-- tools box -->

                            <!-- /. tools -->
                        </div>
                        <div class="box box-body">
                            <div class="progress-group">
                                <span class="progress-text">Si: </span>
                                <span class="progress-number"><b>{{ $participacionSecciones["SI"] }}</b>/{{ $secciones_cantidad }}</span>

                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-green" style="width: {{ $participacionSecciones["SI"]/$secciones_cantidad*100  }}%"></div>
                                </div>
                            </div>
                            <!-- /.progress-group -->
                            <div class="progress-group">
                                <span class="progress-text">No: </span>
                                <span class="progress-number"><b>{{ $participacionSecciones["NO"] }}</b>/{{ $secciones_cantidad }}</span>

                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-red" style="width: {{ $participacionSecciones["NO"]/$secciones_cantidad*100 }}%"></div>
                                </div>
                            </div>
                            <!-- /.progress-group -->
                            <div class="progress-group">
                                <span class="progress-text">Sin Definir</span>
                                <span class="progress-number"><b>{{ $participacionSecciones["SD"] }}</b>/{{ $secciones_cantidad }}</span>
                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-yellow" style="width: {{ $participacionSecciones["SD"]/$secciones_cantidad*100}}%"></div>
                                </div>
                            </div>
                            <!-- /.progress-group -->
                            <div class="progress-group">
                                <span class="progress-text">No Corresponde</span>
                                <span class="progress-number"><b>{{ $participacionSecciones["NC"] }}</b>/{{ $secciones_cantidad }}</span>

                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-aqua" style="width: {{ $participacionSecciones["NC"]/$secciones_cantidad*100 }}%"></div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-md-4">
                    <div class="box box-danger">
                        <div class="box-header ui-sortable-handle" style="cursor: move;">
                            <i class="fa fa-graduation-cap"></i>

                            <h3 class="box-title"><strong>Estudiantes</strong></h3>


                        </div>
                        <div class="box box-body">
                            <div class="table-responsive">
                                <table class="table no-margin">

                                    <tbody>
                                    <tr>

                                        <td>Matrícula</td>
                                        <td><span class="label label-danger">{{ $participacionSecciones["matricula"] }}</span></td>

                                    </tr>
                                    <tr>
                                        <td>Matrícula a Evaluar</td>
                                        <td><span class="label label-warning">{{ $participacionSecciones["matriculaaevaluar"] }}</span></td>

                                    </tr>
                                    <tr>
                                        <td>Matrícula Evaluada</td>
                                        <td><span class="label label-success">{{ $participacionSecciones["matriculaevaluada"] }}</span></td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>


            </div>

            {{--Los establecimientos de esa cabecera--}}

            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Establecimientos</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>CUE</th>
                                        <th>Nombre</th>
                                        <th>Localidad</th>
                                        <th>Participación</th>
                                        <th>Veedor</th>
                                        <th>Falta Aplic.?</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($establecimientos as $establecimiento)
                                        <tr>
                                            <td>{{ $establecimiento->cue }}</td>
                                            <td>{{ $establecimiento->nombre }}</td>
                                            <td>{{ $establecimiento->localidad->descripcion }}</td>
                                            @if($establecimiento->pivot->participacion_id==1)
                                                <td><span class="label label-success">SI</span></td>
                                            @else
                                                    @if($establecimiento->pivot->participacion_id==2)
                                                        <td><span class="label label-danger">NO</span></td>
                                                    @else
                                                        @if($establecimiento->pivot->participacion_id==3)
                                                            <td><span class="label label-warning">Sin Definir</span></td>
                                                        @else
                                                            <td><span class="label label-info">No Corresponde</span></td>
                                                        @endif
                                                    @endif
                                            @endif
                                            <td>{{ $establecimiento->pivot->veedor }}</td>
                                            <td>
                                                @forelse($secciones as $seccion)
                                                    @if($seccion->pivot->establecimiento_id==$establecimiento->id)
                                                        @if($seccion->pivot->participacion_id=="1" && $seccion->pivot->aplicador=="")
                                                            <span class="label label-danger">SI</span>
                                                            @break

                                                         @endif
                                                    @endif
                                                @empty
                                                                <span class="label">No</span>
                                                @endforelse
                                            </td>
                                            <td >

                                                <a href="{{ route('operativo.actualizarEstablecimiento',['operativo' => $operativo, 'cabecera' => $cabecera, 'establecimiento'=>$establecimiento]) }}" class="btn btn-info" title="Actualizar"><i class="fa fa-edit"></i></a>

                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td >
                                                No hay registros configurados.
                                            </td>

                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>

                            </div>
                            <!-- /.box-footer -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </div>

        </div>
    </form>

@endsection


