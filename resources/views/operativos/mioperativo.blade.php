@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{$title}}
@endsection

@section('contentheader_title')
    {{ old('nombre', $operativo->nombre) }}
@endsection

@section('main-content')
    <form method="post" action="{{ url("operativos.{operativo->id}")}}">
        {{ method_field('PUT') }}
        {{csrf_field()}}
        <div class="container-fluid spark-screen">

            <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3>{{$participacionEstablecimientos['SI'] }} / {{ $establecimientos_cantidad }}</h3>

                            <p>Establecimientos</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-university"></i>
                        </div>
                        <a href="#" class="small-box-footer">Nivel Primario: {{ $establecimientosnivelprimario }}</a>
                        <a href="#" class="small-box-footer">Nivel Secundario:  {{ $establecimientosnivelsecundario }}</a>
                    </div>
                </div>


                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3>{{ $cabeceras->count() }}</h3>

                            <p>Cabeceras</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-tag"></i>
                        </div>
                        <a href="#" class="small-box-footer">Sin Coordinador <i class="fa fa-arrow-circle-right"></i></a>
                        <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3>  {{$participacionSecciones['SI'] }} / {{$secciones_cantidad }}</h3>

                            <p>Secciones</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-thumb-tack"></i>
                        </div>
                        <a href="#" class="small-box-footer">Nivel Primario:  {{ $seccionesnivelprimario }}</a>
                        <a href="#" class="small-box-footer">Nivel Secundario:  {{ $seccionesnivelsecundario }}</a>
                    </div>
                </div>


                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3>{{$participacionSecciones['matriculaSI'] }} / {{$participacionSecciones['matriculaTotal'] }}</h3>

                            <p>Estudiantes</p>
                        </div>
                        <div class="icon">
                            <i class="fa fa-graduation-cap"></i>
                        </div>
                        <a href="#" class="small-box-footer">Nivel Primario: {{ $alumnosnivelprimario }}</a>
                        <a href="#" class="small-box-footer">Nivel Secundario: {{ $alumnosnivelsecundario }}</a>
                    </div>
                </div>
                <!-- ./col -->
            </div>


            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Nro Cabecera</th>
                        <th>Localidad</th>
                        <th>Nombre Coordinador</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($cabeceras as $cabecera)
                        <tr>
                            <td>{{ $cabecera->descripcion }}</td>
                            <td>{{ $cabecera->localidad }}</td>
                            @if ($cabecera->user!=null)
                                <td>{{ $cabecera->user->nombre . " " . $cabecera->user->apellido }}</td>
                            @else
                                <td></td>
                            @endif

                            <td> <a href="{{ route('operativo.mostrarDatosCabecera',['operativo' => $operativo, 'cabecera' => $cabecera ] ) }}" class="btn btn-info" title="Modificar"><i class="fa fa-edit"></i></a></td>
                        </tr>
                    @empty
                        <tr>
                            <td >
                                No hay registros configurados.
                            </td>

                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>

            <div class="row pull-right">
                <button type="submit" class="btn btn-primary">Guardar</button>
                <a href="{{ url('operativos') }}" class="btn btn-info">Volver</a>
            </div>
        </div>
    </form>

@endsection

