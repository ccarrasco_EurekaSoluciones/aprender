
@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{$title}}
@endsection

@section('contentheader_title')
    {{$title}}
@endsection

@section('main-content')
    <form method="post" action="{{ url('niveles') }}">
        {{csrf_field()}}
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="form-group">
                <label for="txDescripcion">Descripcion</label>
                <input type="text" id="txDescripcion" name="descripcion" class="form-control"
                       placeholder="Ingrese descripcion del Nivel"  required>
            </div>
        </div>
        <div class="row pull-right">
            <button type="submit" class="btn btn-primary">Guardar</button>
            <a href="{{ url('niveles') }}" class="btn btn-info">Volver</a>
        </div>
    </div>
    </form>

@endsection