
@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{$title}}
@endsection

@section('contentheader_title')

@endsection

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                        <div class="col-md-10">{{$title}}
                        </div>
                        <div class="col-md-2"><a href="{!! url('sectores\nuevo')  !!}"><i class='fa fa-plus-square'></i><span> Nuevo</span></a></li>

                        </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="box-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Descripcion</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($sectores as $sector)
                                        <tr>
                                            <td>{{ $sector->descripcion }}</td>
                                            <td >

                                                <a href="{{ route('sectores.edit', $sector) }}" class="btn btn-info" title="Modificar"><i class="fa fa-edit"></i></a>
                                                <button type="button" class="btn btn-danger"><i class="fa fa-trash"></i></button>

                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td >
                                                No hay registros configurados.
                                            </td>

                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div><!-- /.box-body -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection