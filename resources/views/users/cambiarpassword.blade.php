@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{$title}}
@endsection

@section('contentheader_title')
    {{$title}}
@endsection

@section('main-content')

    @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif


    {!!Form::model($user, ['method' => 'POST', 'url' => "/users/guardarcambiocontrasenia"]) !!}
    <input type="hidden" name="_method" value="POST">
    {{csrf_field()}}



    <div class="box box-default">
        <div class="box-info">
            <div class="box-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-md-1">
                            <label for="login">Login</label>
                        </div>
                        <div class="col-md-3">
                            {!! Form::text('login', $user->login, ['class'=>'form-control', 'readonly']) !!}
                        </div>

                    </div>

                    <div class="form-group">
                        <div class="col-md-1">
                            <label for="apellido" class="control-label">Apellido</label>
                        </div>
                        <div class="col-md-4">
                            {!! Form::text('apellido', $user->apellido, ['class'=>'form-control' , 'readonly']) !!}
                        </div>

                        <div class="col-md-2">
                            <label for="nombre" class="control-label">Nombre</label>
                        </div>
                        <div class="col-md-4">
                            {!! Form::text('nombre', $user->nombre, ['class'=>'form-control' , 'readonly']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-1">
                            <label for="dni" class="control-label">Nueva Contraseña</label>
                        </div>
                        <div class="col-md-4">
                            {!! Form::password('contrasenia', null, ['class'=>'form-control' , 'required']) !!}
                        </div>

                        <div class="col-md-2">
                            <label for="cuit" class="control-label">Confirme Contraseña</label>
                        </div>
                        <div class="col-md-4">
                            {!! Form::password('contraseniaconfirmada', null, ['class'=>'form-control', 'required' ]) !!}
                        </div>
                    </div>


                    <div class="row clearfix pull-right">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <a href="{{ url('home') }}" class="btn btn-info">Volver</a>
                    </div>
                    <div class="row clearfix pull-right">

                    </div>
                </div>
            </div>
        </div>
    </div>

    </form>

@endsection


<script src="{{asset('js/app.js')}}"></script>

