
@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{$title}}
@endsection

@section('contentheader_title')
    {{$title}}
@endsection


@section('main-content')
    <form method="post" action="{{ url('users') }}">
        {{csrf_field()}}


        <div class="box box-default">
            <div class="box-info">
                <div class="box-body">
                    <div class="form-horizontal">

                        <div class="form-group">
                            <div class="col-md-1">
                                <label for="login">Login</label>
                            </div>
                            <div class="col-md-3">
                                {!! Form::text('login', null, ['class'=>'form-control', 'required']) !!}
                            </div>



                            <div class="col-md-2">
                                <label for="esAdmin">Es Administrador?</label>
                            </div>
                            <div class="col-md-2">
                                {!! Form::select('esAdmin', array('0' => 'NO', '1' => 'SI'), false, [ 'class'=>'form-control',  'id'=>'esAdmin' , 'onchange'=>'modificarCabecera();']) !!}

                            </div>

                            <div class="col-md-1">
                                <label for="cabecera" class="control-label">Cabecera</label>
                            </div>
                            <div class="col-md-3">
                                {!! Form::select('cabecera', [null=>'Seleccione Cabecera'] + $cabeceras,  null, ['class' => 'form-control', 'id'=>'cabecera']) !!}
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-1">
                                <label for="apellido" class="control-label">Apellido</label>
                            </div>
                            <div class="col-md-4">
                                {!! Form::text('apellido', null, ['class'=>'form-control' , 'required']) !!}
                            </div>

                            <div class="col-md-2">
                                <label for="nombre" class="control-label">Nombre</label>
                            </div>
                            <div class="col-md-4">
                                {!! Form::text('nombre', null, ['class'=>'form-control' , 'required']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-1">
                                <label for="dni" class="control-label">DNI</label>
                            </div>
                            <div class="col-md-4">
                                {!! Form::text('dni', null, ['class'=>'form-control' , 'required']) !!}
                            </div>

                            <div class="col-md-2">
                                <label for="cuit" class="control-label">CUIT</label>
                            </div>
                            <div class="col-md-4">
                                {!! Form::text('cuit', null, ['class'=>'form-control' ]) !!}
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-1">
                                <label for="direccion">Domicilio</label>
                            </div>
                            <div class="col-md-4">
                                {!! Form::text('direccion', null, ['class'=>'form-control' , 'required']) !!}
                            </div>

                            <div class="col-md-2">
                                <label for="telefono">Telefono</label>
                            </div>
                            <div class="col-md-4">
                                {!! Form::text('telefono', null, ['class'=>'form-control' ])!!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-1">
                                <label for="mail">E-Mail</label>
                            </div>
                            <div class="col-md-7">
                                {!! Form::email('email', null, ['class' => 'form-control', 'required']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-1">
                                <label for="cbu" class="control-label">CBU</label>
                            </div>
                            <div class="col-md-4">
                                {!! Form::text('cbu', null, ['class'=>'form-control' ]) !!}
                            </div>

                            <div class="col-md-2">
                                <label for="numeroempleado">Nro. Empleado</label>
                            </div>
                            <div class="col-md-3">
                                {!! Form::text('numeroempleado', null, ['class'=>'form-control' , 'required']) !!}
                            </div>
                        </div>






                    <div class="row clearfix pull-right">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <a href="{{ url('users') }}" class="btn btn-info">Volver</a>
                    </div>
                    <div class="row clearfix pull-right">

                    </div>
                    </div>
                </div>
            </div>
        </div>


    </form>

@endsection


<script src="{{asset('js/app.js')}}"></script>
<script type="text/javascript">

    function modificarCabecera()
    {

        var valorseleccionado=document.getElementById('esAdmin').value;
        if (valorseleccionado=='0')
        {
            document.getElementById('cabecera').disabled = false;
        }

        else
        {
            document.getElementById('cabecera').disabled = true;
        }
        // alert (valorseleccionado.value);
    }


</script>

