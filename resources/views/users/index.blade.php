
@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{$title}}
@endsection

@section('contentheader_title')

@endsection

@section('main-content')

    @if (session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
    @endif
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-10">{{$title}}
                            </div>
                            <div class="col-md-2"><a href="{!! url('users\nuevo')  !!}"><i class='fa fa-plus-square'></i><span> Nuevo</span></a></li>

                            </div>
                        </div>
                    </div>
                    {!!Form::model(Request::all(),['route' => ['users'], 'method' => 'GET', 'role'=>'search'])!!}
                    <div class="panel-body">

                        <div class="row">

                                <div class="form-group">
                                    <label for="nombre" class="col-md-1 control-label">Nombre</label>
                                    <div class="col-md-3">
                                        {!! Form::text('nombre', null, ['class'=>'form-control', 'placeholder'=>'Nombre']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="apellido" class="col-md-1 control-label">Apellido</label>
                                    <div class="col-md-3">
                                        {!! Form::text('apellido', null, ['class'=>'form-control', 'placeholder'=>'Apellido']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="dni" class="col-md-1 control-label">DNI</label>
                                    <div class="col-md-3">
                                        {!! Form::text('dni', null, ['class'=>'form-control', 'placeholder'=>'DNI']) !!}
                                    </div>
                                </div>
                        </div>
                        <div class="row">
                            <div class="form-group pull-right">
                                <button type="submit" class="btn btn-default">Buscar</button>
                            </div>
                        </div>
                    </div>
                        {!!Form::close() !!}
                        <div class="box-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Login</th>
                                        <th>Apellido</th>
                                        <th>Nombre</th>
                                        <th>EsAdmin</th>
                                        <th>DNI</th>
                                        <th>Cabecera Asig.</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($users as $user)
                                        <tr>
                                            <td>{{ $user->login }}</td>
                                            <td>{{ $user->apellido }}</td>
                                            <td>{{ $user->nombre }}</td>
                                            <td>{{ $user->esAdmin}}</td>
                                            <td>{{ $user->dni }}</td>
                                            @if($user->cabecera !=null)
                                                <td>{{ $user->cabecera->descripcion }}</td>
                                            @else
                                                <td></td>
                                            @endif
                                            <td >

                                                <a href="{{ route('users.edit', $user) }}" class="btn btn-info" title="Modificar"><i class="fa fa-edit"></i></a>
                                                <button type="button" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                                <a href="{{ route('users.resetpassword', $user) }}" class="btn btn-warning" title="Resetear Contraseña"><i class="fa fa-lock"></i></a>

                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td >
                                                No hay registros configurados.
                                            </td>

                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                                {!! $users->render() !!}
                            </div>
                        </div><!-- /.box-body -->

                </div>
            </div>
        </div>
    </div>
@endsection