<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

{{--        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    --}}{{--<img src="{{ Gravatar::get($user->email) }}" class="img-circle" alt="User Image" />--}}{{--
                </div>
                <div class="pull-left info">
                    --}}{{--<p style="overflow: hidden;text-overflow: ellipsis;max-width: 160px;" data-toggle="tooltip" title="{{ Auth::user()->name }}">{{ Auth::user()->name }}</p>--}}{{--
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('adminlte_lang::message.online') }}</a>
                </div>
            </div>
        @endif--}}



        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">{{ trans('adminlte_lang::message.header') }}</li>
            <!-- Optionally, you can add icons to the links -->
            <li class="active"><a href="{{ url('home') }}"><i class='fa fa-link'></i> <span>{{ trans('adminlte_lang::message.home') }}</span></a></li>

             @foreach( auth()->user()->operativos() as $operativo)
                <li><a href="{{ route('aprender', $operativo) }}"><i class='fa fa-mortar-board'></i> <span>{{$operativo->nombre}}</span></a></li>
            @endforeach

            @admin
            <li><a href="{!! url('establecimientos')  !!}"><i class='fa fa-university'></i> <span>Establecimientos</span></a></li>
            <li><a href="{!! url('operativos')  !!}"><i class='fa fa-paperclip'></i> <span>Operativos</span></a></li>
            <li class="treeview">
                <a href="#"><i class='fa fa-gears'></i> <span>Configuración</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">

                    <li><a href="{!! url('niveles')  !!}">Niveles</a></li>
                    <li><a href="{!! url('cabeceras')  !!}">Cabeceras</a></li>
                    <li><a href="{!! url('modalidades')  !!}">Modalidades</a></li>
                    <li><a href="{!! url('sectores')  !!}">Sectores</a></li>
                    <li><a href="{!! url('ambitos')  !!}">Ambitos</a></li>
                    <li><a href="{!! url('periodosfuncionamiento')  !!}">Periodos Func.</a></li>
                    <li><a href="{!! url('localidades')  !!}">Localidades</a></li>
                    <li><a href="{!! url('departamentos')  !!}">Departamentos</a></li>

                </ul>
            </li>

            @endadmin
            <li class="treeview">
                <a href="#"><i class='fa fa-lock'></i> <span>Seguridad</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    @admin
                    <li><a href="{!! url('users')  !!}">Usuarios</a></li>
                    @endadmin
                    <li><a href="{{route('users.cambiarpassword')}}">Cambiar Contraseña</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#"><i class='fa fa-book'></i> <span>Manuales</span> </a>
                <ul class="treeview-menu">

                    <li><a href="docs/A19ManualCoordinadordeCabecera.pdf" target="_blank">Manual Coordinador Cabecera</a></li>
                    <li><a href="docs/A19ManualdelAplicadorCenso.pdf" target="_blank">Manual Aplicación </a></li>
                    <li><a href="docs/A19ManualdelVeedorCenso.pdf" target="_blank">Manual del Veedor</a></li>
                    <li><a href="docs/A19ManualAplicadorMuestra.pdf" target="_blank">Manual Aplicación (Muestra)</a></li>
                    <li><a href="docs/A19ManualVeedorMuestra.pdf" target="_blank">Manual del Veedor (Muestra)</a></li>

                </ul>
            </li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
