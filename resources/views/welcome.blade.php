@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{$title}}
@endsection


@section('contentheader_title')

    <h3 class="box-title">{{$operativo->nombre}}
    </h3>

    <a href="{{ route('aprender', $operativo) }}" class="btn btn-block btn-social btn-dropbox">
        <i class="fa fa-mortar-board"></i>  <b>Ir al operativo</b> </a>

@endsection


@section('main-content')
    <form method="post" action="{{ url("operativos.{operativo->id}")}}">
        {{ method_field('PUT') }}
        {{csrf_field()}}
        <div class="container-fluid spark-screen">

            <div class="row">
                <div class="col-lg-2 col-xs-2">
                    <a href="{{ route('operativo.ExportarCabeceras',['operativo' => $operativo]) }}" >

                        <img src={{ asset('/img/Excel-icon.png') }} alt=""> <br>Cabeceras
                    </a>
                </div>
                <div class="col-lg-2 col-xs-2">
                    <a href="{{ route('operativo.ExportarEstablecimientosAdmin',['operativo' => $operativo]) }}" >

                        <img src={{ asset('/img/Excel-icon.png') }} alt=""> <br>Universo de Establecimientos
                    </a>
                </div>
                <div class="col-lg-2 col-xs-2">
                    <a href="{{ route('operativo.ExportarVeedoresAdmin',['operativo' => $operativo]) }}" >

                        <img src={{ asset('/img/Excel-icon.png') }} alt=""> <br> Listado Veedores
                    </a>
                </div>
                <div class="col-lg-2 col-xs-2">
                    <a href="{{ route('operativo.ExportarAplicadoresAdmin',['operativo' => $operativo]) }}" >

                        <img src={{ asset('/img/Excel-icon.png') }} alt=""> <br>Listado Aplicadores
                    </a>
                </div>
                <div class="col-lg-2 col-xs-2">
                    <a href="{{ route('operativo.ExportarParticipacionAdmin',['operativo' => $operativo]) }}" >

                        <img src={{ asset('/img/Excel-icon.png') }} alt=""> <br>Participación
                    </a>
                </div>
                <div class="col-lg-2 col-xs-2">
                    <a href="{{ route('operativo.ExportarPagos',['operativo' => $operativo]) }}" >

                        <img src={{ asset('/img/Excel-icon.png') }} alt=""> <br>Pagos
                    </a>
                </div>

                <div class="col-lg-2 col-xs-2">
                    <a href="{{ route('operativo.ExportarParticipacionEstadisticas',['operativo' => $operativo]) }}" >

                        <img src={{ asset('/img/Excel-icon.png') }} alt=""> <br>ESTADISTICAS
                    </a>
                </div>

            </div>


        </div>
    </form>

{{--
    {{ auth()->user()->login}}

    @admin
    hola soy un admin
    @endadmin

    {{ auth()->user()->operativos()}}
--}}

@endsection


