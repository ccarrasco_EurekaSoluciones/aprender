@extends('adminlte::layouts.app')

@section('htmlheader_title')
    {{$title}}
@endsection


@section('contentheader_title')

    <h3 class="box-title">{{$operativo->nombre}}
    </h3>

    @if ($cabecera !=null)
    <h4 class="box-title">Cabecera: <b> {{$cabecera->descripcion}} </b>  - Coordinador: <b>{{auth()->user()->nombre}}  {{auth()->user()->apellido}}</b>  </h4>

    <a href="{{ route('aprender', $operativo) }}" class="btn btn-block btn-social btn-dropbox">
        <i class="fa fa-mortar-board"></i>  <b>Ir a mi Cabecera</b> </a>
    @endif
@endsection

@section('main-content')
    <form method="post" action="{{ url("operativos.{operativo->id}")}}">
        {{ method_field('PUT') }}
        {{csrf_field()}}
        <div class="container-fluid spark-screen">

        </div>
    </form>


@section('main-content')

    @if ($cabecera !=null)


    {!!Form::model($operativo, ['method' => 'POST', 'url' => 'operativos/guardaractualizacioncabecera'])!!}
    <input type="hidden" name="_method" value="POST">
    {!! Form::hidden('operativo_cabecera_id', $operativocabecera->id) !!}
    {{csrf_field()}}

    <div class="box box-default">
        <div class="box-info">
            <div class="box-body">
                <div class="form-horizontal">

                    <div class="form-group">
                        <div class="col-md-2">
                            <label for="nombre">Nombre Cabecera</label>
                        </div>
                        <div class="col-md-2">
                            {!! Form::text('nombre', $cabecera->descripcion, ['class'=>'form-control', 'required', 'disabled'=>'true'] ) !!}
                        </div>


                        <div class="col-md-2">
                            <label for="ubicacion">Ubicacion</label>
                        </div>
                        <div class="col-md-6">
                            {!! Form::text('ubicacion', $operativocabecera->ubicacion, ['class'=>'form-control' , 'required']) !!}

                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-2">
                            <label for="direccion">Dirección</label>
                        </div>
                        <div class="col-md-4">
                            {!! Form::text('direccion', $operativocabecera->direccion, ['class'=>'form-control' , 'required']) !!}
                        </div>

                        <div class="col-md-2">
                            <label for="codigopostal">Cod. Postal</label>
                        </div>
                        <div class="col-md-2">
                            {!! Form::text('codigopostal', $operativocabecera->codigopostal, ['class'=>'form-control' ])!!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-2">
                            <label for="localidad" class="control-label">Localidad</label>
                        </div>
                        <div class="col-md-4">
                            {!! Form::select('localidad', [null=>'Seleccione localidad...'] + $localidades, $operativocabecera->localidad_id, ['class' => 'form-control']) !!}
                        </div>

                        <div class="col-md-2">
                            <label for="departamento" class="control-label">Departamento</label>
                        </div>
                        <div class="col-md-4">
                            {!! Form::select('departamento',  [null=>'Seleccione Departamento...'] + $departamentos, $operativocabecera->departamento_id, ['class' => 'form-control']) !!}

                        </div>
                    </div>




                    <div class="form-group pull-right">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                     </div>

                </div>
            </div>
        </div>


    </div>


    </form>



    <div class="row">
        <div class="col-lg-3 col-xs-3">
            <a href="{{ route('operativo.ExportarEstablecimientos',['operativo' => $operativo, 'cabecera' => $cabecera]) }}" >

                <img src={{ asset('/img/Excel-icon.png') }} alt=""> <br>Universo de Establecimientos
            </a>
        </div>
        <div class="col-lg-3 col-xs-3">
            <a href="{{ route('operativo.ExportarVeedores',['operativo' => $operativo, 'cabecera' => $cabecera]) }}" >

                <img src={{ asset('/img/Excel-icon.png') }} alt=""> <br>Listado Veedores
            </a>
        </div>
        <div class="col-lg-3 col-xs-3">
            <a href="{{ route('operativo.ExportarAplicadores',['operativo' => $operativo, 'cabecera' => $cabecera]) }}" >

                <img src={{ asset('/img/Excel-icon.png') }} alt=""> <br>Listado Aplicadores
            </a>
        </div>
        <div class="col-lg-3 col-xs-3">
            <a href="{{ route('operativo.ExportarParticipacion',['operativo' => $operativo, 'cabecera' => $cabecera]) }}" >

                <img src={{ asset('/img/Excel-icon.png') }} alt=""> <br>Participación
            </a>
        </div>
    </div>
    @endif
@endsection


    {{--
        {{ auth()->user()->login}}

        @admin
        hola soy un admin
        @endadmin

        {{ auth()->user()->operativos()}}
    --}}

