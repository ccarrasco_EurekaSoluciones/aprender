<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/home', 'OperativosController@welcome');
Route::get('/', 'OperativosController@welcome')->name('welcome');*/


Route::get('/home', 'UserController@welcome');
Route::get('/', 'UserController@welcome')->name('welcome');



Route::get('/users',  'UserController@index')->name('users');
Route::get('/users/nuevo', 'UserController@create')->name('users.create');
Route::post('/users', 'UserController@store');
Route::get('/users/{user}/editar', 'UserController@resetpassword')->name('users.resetpassword');
Route::get('/users/{user}/resetpassword', 'UserController@edit')->name('users.edit');
Route::put('/users/{user}', 'UserController@update');
Route::get('/users/exportar', 'UserController@export')->name('users.exportar');
Route::get('/users/cambiarPassword','UserController@cambiarpassword')->name('users.cambiarpassword');
Route::post('/users/guardarcambiocontrasenia', 'UserController@guardarCambioPassword');

/*Route::get('/aprender', 'OperativosController@index')->name('operativos');*/

Route::get('/aprender/{operativo}', 'OperativosController@mostrarOperativo')->name('aprender');

Route::get('/operativos', 'OperativosController@index')->name('operativos');;
Route::get('/operativos/nuevo', 'OperativosController@create')->name('operativos.create');
Route::post('/operativos', 'OperativosController@store');
Route::post('/operativos/actualizarseccion',  'OperativosController@actualizarSeccion');
Route::post('/operativos/guardaractualizacionestablecimiento',  'OperativosController@guardarActualizacionEstablecimiento')->name('operativo.guardaractualizacionestablecimiento');
Route::get('/operativos/{operativo}/editar', 'OperativosController@edit')->name('operativos.edit');
Route::put('/operativos/{operativo}', 'OperativosController@update');
Route::get('/operativos/{operativo}/{cabecera}/mostrardatos',  'OperativosController@mostrarDatosCabecera')->name('operativo.mostrarDatosCabecera');
Route::get('/operativos/{operativo}/{cabecera}/{establecimiento}/actualizar',  'OperativosController@actualizarEstablecimiento')->name('operativo.actualizarEstablecimiento');
Route::post('/operativos/guardaractualizacioncabecera', 'OperativosController@guardarActualizacionCabecera')->name('operativo.guardaractualizacioncabecera');

Route::get('/operativos/{operativo}/{cabecera}/exportarEstablecimientos',  'OperativosController@ExportarOperativoEstablecimientos')->name('operativo.ExportarEstablecimientos');
Route::get('/operativos/{operativo}/exportarEstablecimientos',  'OperativosController@ExportarOperativoEstablecimientosAdmin')->name('operativo.ExportarEstablecimientosAdmin');
Route::get('/operativos/{operativo}/{cabecera}/exportarVeedores',  'OperativosController@ExportarOperativoVeedores')->name('operativo.ExportarVeedores');
Route::get('/operativos/{operativo}/exportarVeedores',  'OperativosController@ExportarOperativoVeedoresAdmin')->name('operativo.ExportarVeedoresAdmin');
Route::get('/operativos/{operativo}/{cabecera}/exportarAplicadores',  'OperativosController@ExportarOperativoAplicadores')->name('operativo.ExportarAplicadores');
Route::get('/operativos/{operativo}/exportarAplicadores',  'OperativosController@ExportarOperativoAplicadoresAdmin')->name('operativo.ExportarAplicadoresAdmin');
Route::get('/operativos/{operativo}/{cabecera}/exportarParticipacion',  'OperativosController@ExportarOperativoParticipacion')->name('operativo.ExportarParticipacion');
Route::get('/operativos/{operativo}/exportarParticipacion',  'OperativosController@ExportarOperativoParticipacionAdmin')->name('operativo.ExportarParticipacionAdmin');
Route::get('/operativos/{operativo}/exportarCabeceras',  'OperativosController@ExportarOperativoCabeceras')->name('operativo.ExportarCabeceras');
Route::get('/operativos/{operativo}/exportarPagos',  'OperativosController@ExportarOperativoPagos')->name('operativo.ExportarPagos');
Route::get('/operativos/{operativo}/exportarParticipacionEstadisticas',  'OperativosController@ExportarOperativoEstadisticasParticipacionAdmin')->name('operativo.ExportarParticipacionEstadisticas');


Route::get('/establecimientos',  'EstablecimientosController@index')->name('establecimientos');
Route::get('/establecimientos/nuevo', 'EstablecimientosController@create')->name('establecimientos.create');
Route::post('/establecimientos', 'EstablecimientosController@store');
Route::get('/establecimientos/{establecimiento}/editar', 'EstablecimientosController@edit')->name('establecimientos.edit');
Route::put('/establecimientos/{establecimiento}', 'EstablecimientosController@update');

Route::get('/niveles', 'NivelesController@index')->name('niveles');;
Route::get('/niveles/nuevo', 'NivelesController@create')->name('niveles.create');
Route::post('/niveles', 'NivelesController@store');
Route::get('/niveles/{nivel}/editar', 'NivelesController@edit')->name('niveles.edit');
Route::put('/niveles/{nivel}', 'NivelesController@update');

Route::get('/departamentos', 'DepartamentosController@index')->name('departamentos');;
Route::get('/departamentos/nuevo', 'DepartamentosController@create')->name('departamentos.create');
Route::post('/departamentos', 'DepartamentosController@store');
Route::get('/departamentos/{departamento}/editar', 'DepartamentosController@edit')->name('departamentos.edit');
Route::put('/departamentos/{departamento}', 'DepartamentosController@update');

Route::get('/ambitos', 'AmbitosController@index')->name('ambitos');
Route::get('/ambitos/nuevo', 'AmbitosController@create')->name('ambitos.create');
Route::post('/ambitos', 'AmbitosController@store');
Route::get('/ambitos/{ambito}/editar', 'AmbitosController@edit')->name('ambitos.edit');
Route::put('/ambitos/{ambito}', 'AmbitosController@update');

Route::get('/cabeceras', 'CabecerasController@index')->name('cabeceras');
Route::get('/cabeceras/nuevo', 'CabecerasController@create')->name('cabeceras.create');
Route::post('/cabeceras', 'CabecerasController@store');
Route::get('/cabeceras/{cabecera}/editar', 'CabecerasController@edit')->name('cabeceras.edit');
Route::put('/cabeceras/{cabecera}', 'CabecerasController@update');

Route::get('/modalidades', 'ModalidadesController@index')->name('modalidades');
Route::get('/modalidades/nuevo', 'ModalidadesController@create')->name('modalidades.create');
Route::post('/modalidades', 'ModalidadesController@store');
Route::get('/modalidades/{modalidad}/editar', 'ModalidadesController@edit')->name('modalidades.edit');
Route::put('/modalidades/{modalidad}', 'ModalidadesController@update');

Route::get('/periodosfuncionamiento', 'PeriodosFuncionamientoController@index')->name('periodosfuncionamiento');;
Route::get('/periodosfuncionamiento/nuevo', 'PeriodosFuncionamientoController@create')->name('periodosfuncionamiento.create');
Route::post('/periodosfuncionamiento', 'PeriodosFuncionamientoController@store');
Route::get('/periodosfuncionamiento/{periodo}/editar', 'PeriodosFuncionamientoController@edit')->name('periodosfuncionamiento.edit');
Route::put('/periodosfuncionamiento/{periodo}', 'PeriodosFuncionamientoController@update');

Route::get('/sectores', 'SectoresController@index')->name('sectores');
Route::get('/sectores/nuevo', 'SectoresController@create')->name('sectores.create');
Route::post('/sectores', 'SectoresController@store');
Route::get('/sectores/{sector}/editar', 'SectoresController@edit')->name('sectores.edit');
Route::put('/sectores/{sector}', 'SectoresController@update');

Route::get('/localidades', 'LocalidadesController@index')->name('localidades');
Route::get('/localidades/nuevo', 'LocalidadesController@create')->name('localidades.create');
Route::post('/localidades', 'LocalidadesController@store');
Route::get('/localidades/{localidad}/editar', 'LocalidadesController@edit')->name('localidades.edit');
Route::put('/localidades/{localidad}', 'LocalidadesController@update');
